program test_abort

  integer :: i=1, j = 2
  if ( i.ne.j ) then
    print *, "Aborting..."
    call exit()
  end if
  
  print *, "Abort failed."
  
end program test_abort