program hello
implicit none

!----------MPI variables----------------------
include 'mpif.h'
integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8), TDSIZE = 3
integer(i4b)               :: status(MPI_STATUS_SIZE), id
integer(i4b)               :: myid,numprocs,namesize, ierr, errcode
integer(i4b)               :: i, destination
real(r8b)                  :: transdata(TDSIZE), auxdata(TDSIZE) !, finaldata(TDSIZE)

transdata(1) = 0.D0
transdata(2) = 0.D0
transdata(3) = 0.D0

call MPI_INIT(ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

print *, "Hello world from processor rank ", myid, " out of ", numprocs

if ( MOD(numprocs,4).ne.0 ) then
  call MPI_FINALIZE(ierr)
  print *, "Quitting, number of process are not divisible by 4."
  call exit()
end if

particles: do i = myid+1, (100/numprocs)*numprocs, numprocs
  if (myid==0) print *, i, "test"
  
  transdata(1) = transdata(1) + myid
  transdata(2) = transdata(2) + i
  transdata(3) = transdata(3) + myid*i
  if (myid==0) print *, transdata(1), transdata(2), transdata(3)
  
end do particles

if (myid.ne.0) then
  call MPI_SEND(transdata,TDSIZE,MPI_DOUBLE_PRECISION,0,myid,MPI_COMM_WORLD,ierr)
else

  do id=0, numprocs-1
    if (id==0) then
       auxdata(:)=transdata(:)
       transdata(1) = 0
       transdata(2) = 0
       transdata(3) = 0
    else
       call MPI_RECV(auxdata,TDSIZE,MPI_DOUBLE_PRECISION,id,id,MPI_COMM_WORLD,status,ierr)
    endif
    
    transdata(1) = transdata(1)       +auxdata(1)
    transdata(2) = transdata(2)       +auxdata(2)
    transdata(3) = transdata(3)       +auxdata(3)
  end do
  
end if

if (myid==0) print *, transdata(1), transdata(2), transdata(3)

!call MPI_SEND(transdata,TDSIZE,MPI_DOUBLE_PRECISION,0, myid, MPI_COMM_WORLD, ierr)
!call MPI_RECV(auxdata,TDSIZE,MPI_DOUBLE_PRECISION,id,id,MPI_COMM_WORLD,status,ierr)

call MPI_FINALIZE(ierr)

!---------------------------------------------
end program hello
