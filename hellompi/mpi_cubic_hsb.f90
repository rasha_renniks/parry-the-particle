      program mpi_cubic_hsb
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Monte Carlo simulation of site-diluted Heisenberg Model
!
!     periodic boundary conditions, sequential update of sites
!     uses KISS05 random number generator
!     serial + parallel via conditional compilation
!     -------------------------------------------------------------------
!     History:
!
!     mpi_xy1:     04 June 2015  first version, based on mpi_hw5
!     mpi_xy2:     05 June 2015  performance optimatizations in wolff
!     mpi_xy3:     09 June 2015  fixed bug in error of xis and xit and missing initialization of en2
!     mpi_xy4:     15 June 2015  hot and cold starts
!     mpi_xy5:     15 June 2015  split NMESS for improved estimators
!     mpi_xy6:     18 June 2015  also measure dm/dT directly
!     mpi_xy7:     26 Jan  2016  loop over Lt
!     mpi_xys:     26 Nov  2016  stiffness subroutine
!     mpi_xy8:     10 Dec  2016  Moving stiffness subroutine into the measurement subroutine
!     mpi_hsb:     01 June 2017  Converting XY model into Heisenberg model, making stiffness its own subroutine
!     mpi_cubic_hsb: 20 May 2018  Converted everything to the classical model (3+0)d.
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Preprocessor directives
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#define version 'mpi_cubic_hsb'
#define PARALLEL

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Data types
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      implicit none
      integer,parameter         :: r8b= SELECTED_REAL_KIND(P=14,R=99)    ! 8-byte reals
      integer,parameter         :: i4b= SELECTED_INT_KIND(8)             ! 4-byte integers
      integer,parameter         :: ilog=kind(.true.)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Simulation parameters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      integer(i4b),parameter    :: L=14                                 ! L=linear system size
      real(r8b),   parameter    :: TMAX=1.75D0, TMIN=0.05D0             ! max and min temperatures
      real(r8b),   parameter    :: DT0=0.05D0                           ! temp step, must be positive
      integer(i4b),parameter    :: COLDSTART = -1                       ! set to 1 for cold start and to -1 for hot start

      real(r8b),   parameter    :: IMPCONC=0.65D0                       ! impurtiy concentration
      integer(i4b),parameter    :: NCONF=4000                           ! number of disorder configs
      integer(i4b),parameter    :: NEQ=500,NMESS=1000                   ! Monte Carlo sweeps, must be even

      integer(i4b),parameter    :: IRINIT=1                             ! LFSR seed

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Internal constants - do not touch !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      real(r8b), parameter      :: pi=3.141592653589793D0
      integer(i4b),parameter    :: NTEMP= 1+NINT((TMAX-TMIN)/DT0)       ! number of temperatures
      integer(i4b),parameter    :: TDSIZE=16                            ! size of MPI data transfer array
      integer(i4b),parameter    :: L3 = L*L*L                           ! L^3, cubic system size

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Variable declarations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      real (r8b)      :: sx(0:L3-1),sy(0:L3-1),sz(0:L3-1)            ! Heisenberg spin
      logical(ilog)   :: occu(0:L3-1)                                ! occupation of site with spin
      real (r8b)      :: sxnew, synew, sznew, slen
      real (r8b)      :: nsx,nsy,nsz, dE                             ! sum over neighboring spins

      real(r8b)       :: mx,my,mz,sweepmag,sweepen                   ! magnetization vector
      real(r8b)       :: sweepmagq1,sweepmagq2,sweepmagq3,magqs      ! mag(qspace)
      real(r8b)       :: Gs,Gscon                                    ! G(qspace), connected version
      real(r8b)       :: mag,mag2,mag4,bin,susc,en,en2,sph           ! magnetization, its square, energy
      real(r8b)       :: enmag,dmdT                                  ! <e m>
      real(r8b)       :: mag1half,mag2half,en1half,en2half
      real(r8b)       :: xis,xiscon                                  ! correlation lengths in space, connected versions
      real(r8b)       :: glxis,glxiscon                              ! global correlation lengths in space, connected versions
      real(r8b)       :: stiffx, stiffy, stiffz                      ! stiffness in X,Y and Z
      real(r8b)       :: stx,sty,stz
      real(r8b)       :: sweepstiffx, sweepstiffy, sweepstiffz
      real(r8b)       :: ax,ay                                       ! a vector that is perpendicular to the magnetic field, used for heisenberg stiffness

      real(r8b)       :: confmag(NTEMP),confmag2(NTEMP)
      real(r8b)       :: conf2mag(NTEMP), confmag4(NTEMP)            ! configuration averages
      real(r8b)       :: conflogmag(NTEMP)
      real(r8b)       :: confsusc(NTEMP), confbin(NTEMP)
      real(r8b)       :: conf2susc(NTEMP), conf2bin(NTEMP)           ! conf. av. of squared observables
      real(r8b)       :: confen(NTEMP),confsph(NTEMP)
      real(r8b)       :: conf2en(NTEMP),conf2sph(NTEMP)
      real(r8b)       :: confGs(NTEMP),confGscon(NTEMP)
      real(r8b)       :: confxis(NTEMP)
      real(r8b)       :: conf2xis(NTEMP)
      real(r8b)       :: confxiscon(NTEMP)
      real(r8b)       :: confdmdT(NTEMP),conf2dmdT(NTEMP)
      real(r8b)       :: confdlnmdT(NTEMP),conf2dlnmdT(NTEMP)
      real(r8b)       :: confstiffx(NTEMP),confstiffy(NTEMP),confstiffz(NTEMP)     ! stiffness
      real(r8b)       :: conf2stiffx(NTEMP),conf2stiffy(NTEMP),conf2stiffz(NTEMP)  !stiffness error bars

      integer(i4b)    :: m1(0:L3-1)            ! neighbor table
      integer(i4b)    :: m2(0:L3-1)
      integer(i4b)    :: m3(0:L3-1)
      integer(i4b)    :: m4(0:L3-1)
      integer(i4b)    :: m5(0:L3-1)
      integer(i4b)    :: m6(0:L3-1)

      real(r8b)       :: qspace                ! minimum q values for correlation length


      integer(i4b)    :: iconf,init            ! current disorder config
      integer(i4b)    :: totconf               ! total number of confs run so far
      integer(i4b)    :: N_IMPSITE,iimp        ! number of impurity sites

      integer(i4b)    :: i1,i2,i3              ! coordinates
      integer(i4b)    :: is                    ! site index
      integer(i4b)    :: conc_int              ! concentration as an intebger, used for file header

      integer(i4b)    :: isweep                ! Monte Carlo sweep
      real(r8b)       :: T, dT, beta           ! Monte Carlo temperature
      integer(i4b)    :: itemp

      integer(i4b)    :: nclsweep,nspsweep     ! number of clusters, spins flipped in single sweep
      real(r8b)       :: totncl,totnsp         ! total numbers of clusters, spins flipped
      integer(i4b)    :: ncluster
      real(r8b)       :: avclsize

      real(r8b),external         :: rkiss05
      external                      kissinit

      character avenfile*16,avmafile*16,avcofile*16,avdmfile*16,avstfile*16

! Now the MPI stuff !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

#ifdef PARALLEL
      include 'mpif.h'
      integer(i4b)              :: ierr
      integer(i4b)              :: id,myid                               ! process index
      integer(i4b)              :: numprocs                              ! total number of processes
      integer(i4b)              :: status(MPI_STATUS_SIZE)
      real(r8b)                 :: transdata(TDSIZE),auxdata(TDSIZE)     ! MPI transfer array
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Start of main program
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      conc_int = nint(IMPCONC*1000.D0)

      avenfile='avenC000L000.dat'
      avmafile='avmaC000L000.dat'
      avcofile='avdmC000L000.dat'
      avdmfile='avdmC000L000.dat'
      avstfile='avstC000L000.dat'
      write(avenfile(6:8),'(I3.3)') conc_int
      write(avmafile(6:8),'(I3.3)') conc_int
      write(avcofile(6:8),'(I3.3)') conc_int
      write(avdmfile(6:8),'(I3.3)') conc_int
      write(avstfile(6:8),'(I3.3)') conc_int
      write(avenfile(10:12),'(I3.3)') L
      write(avmafile(10:12),'(I3.3)') L
      write(avcofile(10:12),'(I3.3)') L
      write(avdmfile(10:12),'(I3.3)') L
      write(avstfile(10:12),'(I3.3)') L

! Set up MPI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef PARALLEL
      call MPI_INIT(ierr)
      call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )

      if (myid==0) then
         print *,'Program ', version, ' running on', numprocs, ' processes'
         print *,'--------------------------------------------------'
         print *,'L= ', L
         print *,'MC steps: ', NEQ, ' + ', NMESS
      endif ! of if (myid==0)
#else
      print *,'Program', version, 'running on single processor'
      print *,'--------------------------------------------------'
      print *,'L= ', L
      print *,'MC steps: ', NEQ, ' + ', NMESS
#endif

! Set up neigbor table

      do i1=0, L-1
      do i2=0, L-1
      do i3=0, L-1
          is = L*L*i1 + L*i2 + i3

          if (i1.eq.L-1) then          !testing the boundary conditions, checking if you are in the last layer if yes, then go back to the first layer
              m1(is)=is - L*L*(L-1)    !m1 and m2 are the x-directions (could just as well be the y or z direction, all equivalent)
          else
              m1(is)=is + L*L
          endif

          if (i1.eq.0) then
              m2(is)=is + L*L*(L-1)
          else
              m2(is)=is - L*L
          endif

          if (i2.eq.L-1) then
              m3(is)= is - L*(L-1)      !m3 and m4 are the y-directions
          else
              m3(is)= is + L
          endif

          if (i2.eq.0) then
              m4(is)=is + L*(L-1)
          else
              m4(is)=is - L
          endif

          if (i3.eq.L-1) then           !m5 and m6 are the z-directions
              m5(is)= is - (L-1)
          else
              m5(is)= is + 1
          endif

          if (i3.eq.0) then
              m6(is)= is + (L-1)
          else
              m6(is)= is - 1
          endif
      enddo
      enddo
      enddo


      confmag(:)     = 0.D0
      conf2mag(:)    = 0.D0
      confmag2(:)    = 0.D0
      confmag4(:)    = 0.D0
      conflogmag(:)  = 0.D0
      confsusc(:)    = 0.D0
      confbin(:)     = 0.D0
      conf2susc(:)   = 0.D0
      conf2bin(:)    = 0.D0
      confen(:)      = 0.D0
      confsph(:)     = 0.D0
      conf2en(:)     = 0.D0
      conf2sph(:)    = 0.D0
      confGs(:)      = 0.D0
      confGscon(:)   = 0.D0
      confxis(:)     = 0.D0
      conf2xis(:)    = 0.D0
      confxiscon(:)  = 0.D0
      confdmdT(:)    = 0.D0
      conf2dmdT(:)   = 0.D0
      confdlnmdT(:)  = 0.D0
      conf2dlnmdT(:) = 0.D0
      confstiffx(:)  = 0.D0
      confstiffy(:)  = 0.D0
      confstiffz(:)  = 0.D0
      conf2stiffx(:) = 0.D0
      conf2stiffy(:) = 0.D0
      conf2stiffz(:) = 0.D0

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!  Loop over disorder configurations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef PARALLEL
      disorder_loop: do iconf=myid+1,(NCONF/numprocs)*numprocs,numprocs
         if (myid==0) print *, 'dis. conf.', iconf
#else
      disorder_loop: do iconf=1,NCONF
         print *, 'dis. conf.', iconf
#endif

      N_IMPSITE=nint(L*L*impconc)

! Initialize random number generator, IRINIT must be positive
      init=irinit+iconf-1
      call kissinit(init)

! Initialize impurities
      iimp=0
        occu(:)=.true.
        do is=0,L3-1
          if(rkiss05()<IMPCONC) then
            occu(is)=.false.
            iimp=iimp+1
          endif
        enddo

! Initialize spins
      if (COLDSTART==1) then
        do is=0, L3-1
        if (occu(is)) then
           sx(is)=1.D0          ! Cold start - set all spins in the X-direction
           sy(is)=0.D0
           sz(is)=0.D0
        endif
        enddo
      else
        do is=0, L3-1
        if (occu(is)) then
          slen=1.D0
          do while (slen>0.5D0)
             sx(is)= rkiss05()-0.5D0        ! Hot start - spins are randomly oriented
             sy(is)= rkiss05()-0.5D0
             sz(is)= rkiss05()-0.5D0
             slen=sqrt(sx(is)*sx(is) + sy(is)*sy(is) + sz(is)*sz(is) )
          enddo
          sx(is)=sx(is)/slen
          sy(is)=sy(is)/slen
          sz(is)=sz(is)/slen
        endif
        enddo
      endif

! Loop over temperatures
         if (COLDSTART==1) then
            T=TMIN
            dT=DT0
         else
            T=TMAX
            dT=-DT0
         endif
      temperature: do itemp=1,NTEMP
      beta=1.D0/T
!      print *,'T= ',real(T)

!     Equilibration, carry out NEQ full MC steps

      do isweep=1,NEQ/2
         call metro_sweep
         call wolff_sweep(0,nclsweep,nspsweep)
      enddo
      totncl=0.D0
      totnsp=0.D0
      do isweep=1, NEQ/2
         call metro_sweep
         call wolff_sweep(0,nclsweep,nspsweep)
         totncl=totncl+nclsweep
         totnsp=totnsp+nspsweep
      enddo
      avclsize=totnsp/totncl
      ncluster=L3/avclsize+1

! Measuring loop, carry out NMESS full MC sweeps

      mag    = 0.D0
      mag2   = 0.D0
      mag4   = 0.D0
      en     = 0.D0
      en2    = 0.D0
      magqs  = 0.D0
      Gs     = 0.D0
      enmag  = 0.D0
      stiffx = 0.D0
      stiffy = 0.D0
      stiffz = 0.D0
      totncl = 0.D0
      totnsp = 0.D0
      qspace=2*pi/L        ! minimum q values for correlation length


      do isweep=1, NMESS/2
         call metro_sweep
         call wolff_sweep(ncluster,nclsweep,nspsweep)

         totncl=totncl+nclsweep
         totnsp=totnsp+nspsweep

         call measurement
         call stiffness
         call corr_func
      enddo   ! of do isweep ...
      mag1half=mag
      en1half=en

      call metro_sweep                                ! separate the two halves
      call wolff_sweep(ncluster,nclsweep,nspsweep)

      do isweep=1, NMESS/2
         call metro_sweep
         call wolff_sweep(ncluster,nclsweep,nspsweep)
         totncl=totncl+nclsweep
         totnsp=totnsp+nspsweep

         call measurement
         call stiffness
         call corr_func
      enddo     ! of do isweep ...

      mag2half=mag-mag1half
      en2half=en-en1half
      avclsize=totnsp/totncl

      mag=mag/NMESS
      mag2=mag2/NMESS
      mag4=mag4/NMESS
      en=en/NMESS
      en2=en2/NMESS
      mag1half=2.D0*mag1half/NMESS
      mag2half=2.D0*mag2half/NMESS
      en1half=2.D0*en1half/NMESS
      en2half=2.D0*en2half/NMESS
      enmag=enmag/NMESS
      magqs=magqs/NMESS
      Gs=Gs/NMESS

      susc=(mag2-mag1half*mag2half)*L3*beta
      bin=1-mag4/(3*mag2**2)
      sph=(en2-en1half*en2half)*L3*beta**2
      dmdT=(enmag-0.5*(en1half*mag2half+en2half*mag1half))*L3*beta**2

      Gscon=Gs-magqs**2

      xis= (mag2-Gs)/(Gs*qspace*qspace)
      xis=sqrt(abs(xis))
      xiscon= ((mag2-mag**2)-Gscon)/(Gscon*qspace*qspace)
      xiscon=sqrt(abs(xiscon))

      stiffx = stiffx/NMESS
      stiffy = stiffy/NMESS
      stiffz = stiffz/NMESS


#ifdef PARALLEL
!! Package data for transmission
      transdata(1)=mag
      transdata(2)=mag2
      transdata(3)=mag4
      transdata(4)=en
      transdata(5)=en2
      transdata(6)=susc
      transdata(7)=bin
      transdata(8)=sph
      transdata(9)=Gs
      transdata(10)=Gscon
      transdata(11)=xis
      transdata(12)=xiscon
      transdata(13)=dmdT
      transdata(14)=stiffx
      transdata(15)=stiffy
      transdata(16)=stiffz

      if(myid.ne.0) then                                                  ! Send data
         call MPI_SEND(transdata,TDSIZE,MPI_DOUBLE_PRECISION,0,myid,MPI_COMM_WORLD,ierr)
      else                                                                ! Receive data
         do id=0,numprocs-1
            if (id==0) then
               auxdata(:)=transdata(:)
            else
               call MPI_RECV(auxdata,TDSIZE,MPI_DOUBLE_PRECISION,id,id,MPI_COMM_WORLD,status,ierr)
            endif

            confmag(itemp)=confmag(itemp)       +auxdata(1)
            conf2mag(itemp)=conf2mag(itemp)     +(auxdata(1))**2
            confmag2(itemp)=confmag2(itemp)     +auxdata(2)
            confmag4(itemp)=confmag4(itemp)     +auxdata(3)
            conflogmag(itemp)=conflogmag(itemp) +log(auxdata(1))
            confsusc(itemp)=confsusc(itemp)     +auxdata(6)
            confbin(itemp)=confbin(itemp)       +auxdata(7)
            conf2susc(itemp)=conf2susc(itemp)   +(auxdata(6))**2
            conf2bin(itemp)=conf2bin(itemp)     +(auxdata(7))**2
            confen(itemp)=confen(itemp)         +auxdata(4)
            confsph(itemp)=confsph(itemp)       +auxdata(8)
            conf2en(itemp)=conf2en(itemp)       +(auxdata(4))**2
            conf2sph(itemp)=conf2sph(itemp)     +(auxdata(8))**2
            confGs(itemp)=confGs(itemp)         +auxdata(9)
            confGscon(itemp)=confGscon(itemp)   +auxdata(10)
            confxis(itemp)=confxis(itemp)       +auxdata(11)
            conf2xis(itemp)=conf2xis(itemp)     +(auxdata(11))**2
            confxiscon(itemp)=confxiscon(itemp) +auxdata(12)
            confdmdT(itemp)=confdmdT(itemp)     +auxdata(13)
            conf2dmdT(itemp)=conf2dmdT(itemp)   +(auxdata(13))**2
            confdlnmdT(itemp)=confdlnmdT(itemp) +auxdata(13)/auxdata(1)
            conf2dlnmdT(itemp)=conf2dlnmdT(itemp)+(auxdata(13)/auxdata(1))**2
            confstiffx(itemp)=confstiffx(itemp) + (auxdata(14))
            confstiffy(itemp)=confstiffy(itemp) + (auxdata(15))
            confstiffz(itemp)=confstiffz(itemp) + (auxdata(16))
            conf2stiffx(itemp)=conf2stiffx(itemp) + (auxdata(14))**2
            conf2stiffy(itemp)=conf2stiffy(itemp) + (auxdata(15))**2
            conf2stiffz(itemp)=conf2stiffz(itemp) + (auxdata(16))**2

         enddo
      endif

#else
      confmag(itemp)=confmag(itemp)      +mag
      conf2mag(itemp)=conf2mag(itemp)    +mag**2
      confmag2(itemp)=confmag2(itemp)    +mag2
      confmag4(itemp)=confmag4(itemp)    +mag4
      conflogmag(itemp)=conflogmag(itemp)+log(mag)
      confsusc(itemp)=confsusc(itemp)    +susc
      confbin(itemp)=confbin(itemp)      +bin
      conf2susc(itemp)=conf2susc(itemp)  +susc**2
      conf2bin(itemp)=conf2bin(itemp)    +bin**2
      confen(itemp)=confen(itemp)        +en
      confsph(itemp)=confsph(itemp)      +sph
      conf2en(itemp)=conf2en(itemp)      +en**2
      conf2sph(itemp)=conf2sph(itemp)    +sph**2
      confGs(itemp)=confGs(itemp)        +Gs
      confGscon(itemp)=confGscon(itemp)  +Gscon
      confxis(itemp)=confxis(itemp)       +xis
      conf2xis(itemp)=conf2xis(itemp)     +xis**2
      confxiscon(itemp)=confxiscon(itemp) +xiscon
      confdmdT(itemp)=confdmdT(itemp)     +dmdT
      conf2dmdT(itemp)=conf2dmdT(itemp)   +dmdT**2
      confdlnmdT(itemp)=confdlnmdT(itemp) +dmdT/mag
      conf2dlnmdT(itemp)=conf2dlnmdT(itemp)+(dmdT/mag)**2
      confstiffx(itemp)=confstiffx(itemp) + stiffx
      confstiffy(itemp)=confstiffy(itemp) + stiffy
      confstiffz(itemp)=confstiffz(itemp) + stiffz
      conf2stiffx(itemp)=conf2stiffx(itemp) + stiffx**2
      conf2stiffy(itemp)=conf2stiffy(itemp) + stiffy**2
      conf2stiffz(itemp)=conf2stiffz(itemp) + stiffz**2

#endif

      T=T+dT
      enddo temperature

!!! output !!!!!!!!!!!!!!!!!!!!!!!!

#ifdef PARALLEL
      totconf=iconf+numprocs-1
      if (myid==0) then
#else
      totconf=iconf
#endif

      open(7,file=avenfile,status='replace')
      rewind(7)
      write(7,*) 'program ', version
      write(7,*) 'spatial ', L
      write(7,*) 'equilibration steps     ',  NEQ
      write(7,*) 'measurement steps   ', NMESS
      write(7,*) 'impurity conc.  ', real(impconc),',  number of imps. ',N_IMPSITE
      write(7,*) 'disorder configurations ', NCONF
      write(7,*) 'disorder configurations processed ',totconf,' of  ',NCONF
      write(7,*) 'COLDSTART ',COLDSTART
      write(7,*) 'LFSR-Init        ', IRINIT
      write(7,*) '-----------------'
      write(7,10)
      10    format(7x,'T',9x,'[<energy>]',1x,'std.en/sqrt(totconf)',1x,'[<spec.heat>]',1x,'std.spec.heat/sqrt(totconf)')
      if (COLDSTART==1) then
         T=TMIN
         dT=DT0
      else
         T=TMAX
         dT=-DT0
      endif
      do itemp=1,NTEMP
        write(7,'(1x,25(e13.7,1x))') t,confen(itemp)/totconf,&
                                     (sqrt(conf2en(itemp)/totconf-(confen(itemp)/totconf)**2))/sqrt(1.D0*totconf),&
                                     confsph(itemp)/totconf,&
                                     (sqrt(conf2sph(itemp)/totconf-(confsph(itemp)/totconf)**2))/sqrt(1.D0*totconf)
        T=T+dT
      enddo
      close(7)
      open(7,file=avmafile,status='replace')
      rewind(7)
      write(7,*) 'program ', version
      write(7,*) 'spatial ', L
      write(7,*) 'equilibration steps     ',  NEQ
      write(7,*) 'measurement steps   ', NMESS
      write(7,*) 'impurity conc.  ', real(impconc),',  number of imps. ',N_IMPSITE
      write(7,*) 'disorder configurations ', NCONF
      write(7,*) 'disorder configurations processed ',totconf,' of  ',NCONF
      write(7,*) 'COLDSTART ',COLDSTART
      write(7,*) 'LFSR-Init        ', IRINIT
      write(7,*) '-----------------'
      write(7,11)
      11    format(7x,'T',11x,'[<mag>]',5x,'[<mag>^2]',1x,'std.mag/sqrt(totconf)',1x,'[<susc>]',1x,'std.susc/sqrt(iconf)',1x,&
                    &'[<Binder>]',1x,'std.bin/sqrt(totconf)',1x,'[log <mag>]',1x,'Global.Binder')
      if (COLDSTART==1) then
         T=TMIN
         dT=DT0
      else
         T=TMAX
         dT=-DT0
      endif
      do itemp=1,NTEMP
         write(7,'(1x,25(e13.7,1x))') t,confmag(itemp)/totconf,conf2mag(itemp)/totconf,&
              (sqrt(conf2mag(itemp)/totconf-(confmag(itemp)/totconf)**2))/sqrt(1.D0*totconf),confsusc(itemp)/totconf,&
              (sqrt(conf2susc(itemp)/totconf-(confsusc(itemp)/totconf)**2))/sqrt(1.D0*totconf), confbin(itemp)/totconf,&
              (sqrt(conf2bin(itemp)/totconf-(confbin(itemp)/totconf)**2))/sqrt(1.D0*totconf), conflogmag(itemp)/totconf,&
              1-confmag4(itemp)/totconf/(3*(confmag2(itemp)/totconf)**2)
         T=T+dT
      enddo
      close(7)
      open(7,file=avcofile,status='replace')
      rewind(7)
      write(7,*) 'program ', version
      write(7,*) 'spatial ', L
      write(7,*) 'equilibration steps     ',  NEQ
      write(7,*) 'measurement steps   ', NMESS
      write(7,*) 'impurity conc.  ', real(impconc),',  number of imps. ',N_IMPSITE
      write(7,*) 'disorder configurations ', NCONF
      write(7,*) 'disorder configurations processed ',totconf,' of  ',NCONF
      write(7,*) 'COLDSTART ',COLDSTART
      write(7,*) 'LFSR-Init        ', IRINIT
      write(7,*) '-----------------'
      write(7,12)
      12        format(7x,'T',6x,'[xis]',6x,'[xis]/L',6x,'[xiscon]',6x,&
                        &'[xiscon]/L',6x,'glxis',6x,'glxis/L',6x,'glxiscon',6x,&
                        &'glxiscon/L',6x,'std.dev.(xis/L)')
      if (COLDSTART==1) then
         T=TMIN
         dT=DT0
      else
         T=TMAX
         dT=-DT0
      endif
      do itemp=1,NTEMP
         glxis= (confmag2(itemp) - confGs(itemp))/ (confGs(itemp)*qspace*qspace)
         glxis=sqrt(abs(glxis))
         glxiscon= ((confmag2(itemp) - (confmag(itemp)**2)/totconf) - confGscon(itemp))/ (confGscon(itemp)*qspace*qspace)
         glxiscon=sqrt(abs(glxiscon))

         write(7,'(1x,25(e13.7,1x))') t, confxis(itemp)/totconf, confxis(itemp)/totconf/L,&
              confxiscon(itemp)/totconf,confxiscon(itemp)/totconf/L,&
              glxis,glxis/L,glxiscon,glxiscon/L,&
              (sqrt(conf2xis(itemp)/totconf-(confxis(itemp)/totconf)**2))/sqrt(1.D0*totconf)/L
         T=T+DT
      enddo
      close(7)
      open(7,file=avdmfile,status='replace')
      rewind(7)
      write(7,*) 'program ', version
      write(7,*) 'spatial ', L
      write(7,*) 'equilibration steps     ',  NEQ
      write(7,*) 'measurement steps   ', NMESS
      write(7,*) 'impurity conc.  ', real(impconc),',  number of imps. ',N_IMPSITE
      write(7,*) 'disorder configurations ', NCONF
      write(7,*) 'disorder configurations processed ',totconf,' of  ',NCONF
      write(7,*) 'COLDSTART ',COLDSTART
      write(7,*) 'LFSR-Init        ', IRINIT
      write(7,*) '-----------------'
      write(7,13)
      13    format(7x,'T',11x,'[<m>]',1x,'std.m/sqrt(totconf)',1x,'[<dmdT>]',1x,'std.dmdT/sqrt(totconf)',1x,&
                    &'[<dlnmdT>]',1x,'std.dlnmdT/sqrt(totconf)',1x,'[<dmdT>]/[<m>]')
      if (COLDSTART==1) then
         T=TMIN
         dT=DT0
      else
         T=TMAX
         dT=-DT0
      endif
      do itemp=1,NTEMP
        write(7,'(1x,25(e13.7,1x))') t,confmag(itemp)/totconf,&
              (sqrt(conf2mag(itemp)/totconf-(confmag(itemp)/totconf)**2))/sqrt(1.D0*totconf), confdmdT(itemp)/totconf,&
              (sqrt(conf2dmdT(itemp)/totconf-(confdmdT(itemp)/totconf)**2))/sqrt(1.D0*totconf),confdlnmdT(itemp)/totconf,&
              (sqrt(conf2dlnmdT(itemp)/totconf-(confdlnmdT(itemp)/totconf)**2))/sqrt(1.D0*totconf),&
              confdmdT(itemp)/confmag(itemp)
        T=T+dT
      enddo
      close(7)
      open(7,file=avstfile,status='replace')
      rewind(7)
      write(7,*) 'program ', version
      write(7,*) 'spatial ', L
      write(7,*) 'equilibration steps     ',  NEQ
      write(7,*) 'measurement steps   ', NMESS
      write(7,*) 'impurity conc.  ', real(impconc),',  number of imps. ',N_IMPSITE
      write(7,*) 'disorder configurations ', NCONF
      write(7,*) 'disorder configurations processed ',totconf,' of  ',NCONF
      write(7,*) 'COLDSTART ',COLDSTART
      write(7,*) 'LFSR-Init        ', IRINIT
      write(7,*) '-----------------'
      write(7,14)
      14    format(7x,'T',7x,'[<StiffX>]',1x,'std.stiffx/sqrt(totconf)',1x,'[<StiffY>]',1x,&
                    &'std.StiffY/sqrt(totconf)',1x,'[<StiffZ>]',1x,'std.StiffZ/sqrt(totconf)')
      if (COLDSTART==1) then
         T=TMIN
         dT=DT0
      else
         T=TMAX
         dT=-DT0
      endif
      do itemp=1,NTEMP
        write(7,'(1x,25(e13.7,1x))') t, confstiffx(itemp)/totconf,&
              (sqrt(conf2stiffx(itemp)/totconf-(confstiffx(itemp)/totconf)**2))/sqrt(1.D0*totconf), confstiffy(itemp)/totconf,&
              (sqrt(conf2stiffy(itemp)/totconf-(confstiffy(itemp)/totconf)**2))/sqrt(1.D0*totconf), confstiffz(itemp)/totconf,&
              (sqrt(conf2stiffz(itemp)/totconf-(confstiffz(itemp)/totconf)**2))/sqrt(1.D0*totconf)
        T=T+dT
      enddo
      close(7)

#ifdef PARALLEL
      endif ! of if (myid==0)
#endif

      enddo disorder_loop

#ifdef PARALLEL
      call MPI_FINALIZE(ierr)
#endif

      stop
      contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine Wolff_sweep(ntobeflipped,nclflipped,nspflipped)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!     Performs a Wolff sweep consisting of ncluster single cluster flips
!
!     If ntobeflipped <= 0, subroutine determines length of sweep by counting
!     flipped spins rather than clusters (usefull for equilibration when
!     cluster size not known)
!
!     Do NOT use ntobeflipped <= 0 for measurement because the last cluster
!     flipped to reach L3 flipped spins is biased towards large clusters
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      integer(i4b)    :: ntobeflipped          ! INPUT: number of spins to be flipped
      integer(i4b)    :: nclflipped            ! OUTPUT: number of clusters flipped
      integer(i4b)    :: nspflipped            ! OUTPUT: number of spins flipped

      integer(i4b)    :: stack(0:L3-1)         ! stack for cluster construction
      integer(i4b)    :: sp                    ! stackpointer
      integer(i4b)    :: current,neighbor      ! current site in cluster construction, its neighbor
      integer(i4b)    :: isize                 ! size of current cluster
      real(r8b)       :: nx,ny,nz              ! reflection direction
      real(r8b)       :: scalar1,scalar2, snsn ! scalar products in addition probability
      real(r8b)       :: padd

      nclflipped=0
      nspflipped=0


      cluster_loop: do

         is = int(L3*rkiss05())             ! seed site for Wolff cluster
         if (occu(is)) then                 ! is site occupied?

         stack(0)=is
         sp=1

         slen=1.D0
         do while (slen>0.5D0)
            nx= rkiss05()-0.5D0
            ny= rkiss05()-0.5D0
            nz= rkiss05()-0.5D0
            slen= sqrt(nx*nx+ny*ny+nz*nz)
         enddo
         nx= nx/slen
         ny= ny/slen
         nz= nz/slen

         nclflipped=nclflipped+1
         isize=1
         scalar2= 2.D0*(nx*sx(is)+ny*sy(is)+nz*sz(is))             ! scalar product for p_add

         sx(is)=sx(is)-nx*scalar2                                  ! now flip seed spin
         sy(is)=sy(is)-ny*scalar2
         sz(is)=sz(is)-nz*scalar2

         do while(sp.gt.0)                                         ! now build the cluster
           sp=sp-1
           current = stack(sp)                                     ! get site from stack

               scalar1=- (nx*sx(current)+ny*sy(current)+nz*sz(current))   ! scalar product for p_add

               neighbor=m6(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif

               neighbor=m5(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif

               neighbor=m4(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif

               neighbor=m3(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif

               neighbor=m2(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif

               neighbor=m1(current)
               if (occu(neighbor)) then
               scalar2=  2.D0*(nx*sx(neighbor)+ny*sy(neighbor)+nz*sz(neighbor))    ! scalar product for p_add
               snsn=scalar1*scalar2
               if (snsn>0) then
                  padd=1.D0-exp(-beta*snsn)                        ! check whether parallel
                  if(rkiss05().lt.padd) then
                     sx(neighbor)=sx(neighbor)-nx*scalar2          ! now flip current spin
                     sy(neighbor)=sy(neighbor)-ny*scalar2
                     sz(neighbor)=sz(neighbor)-nz*scalar2
                     stack(sp)=neighbor
                     sp=sp+1
                     isize=isize+1
                  endif
               endif
               endif


            enddo       ! of cluster building and flipping

         nspflipped   = nspflipped   +isize

         endif          ! of if(occu(is))

         if (ntobeflipped>0) then
            if (nclflipped.ge.ntobeflipped) exit cluster_loop
         else
            if (nspflipped.ge.L3) exit cluster_loop
         endif
      enddo  cluster_loop

      end subroutine wolff_sweep


     subroutine metro_sweep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!    carries out one Metropolis sweep
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      real(r8b)    :: angle

      do is=0, L3-1
      if (occu(is)) then
          nsx=0.D0
          nsy=0.D0
          nsz=0.D0
          if (occu(m1(is))) then
             nsx=nsx+sx(m1(is))
             nsy=nsy+sy(m1(is))
             nsz=nsz+sz(m1(is))
          endif
          if (occu(m2(is))) then
             nsx=nsx+sx(m2(is))
             nsy=nsy+sy(m2(is))
             nsz=nsz+sz(m2(is))
          endif
          if (occu(m3(is))) then
             nsx=nsx+sx(m3(is))
             nsy=nsy+sy(m3(is))
             nsz=nsz+sz(m3(is))
          endif
          if (occu(m4(is))) then
             nsx=nsx+sx(m4(is))
             nsy=nsy+sy(m4(is))
             nsz=nsz+sz(m4(is))
          endif
          if (occu(m5(is))) then
             nsx=nsx+sx(m5(is))
             nsy=nsy+sy(m5(is))
             nsz=nsz+sz(m5(is))
          endif
          if (occu(m6(is))) then
             nsx=nsx+sx(m6(is))
             nsy=nsy+sy(m6(is))
             nsz=nsz+sz(m6(is))
          endif

          slen=1.D0
          do while (slen>0.5D0)
             sxnew= rkiss05()-0.5D0
             synew= rkiss05()-0.5D0
             sznew= rkiss05()-0.5D0
             slen=sqrt(sxnew*sxnew + synew*synew + sznew*sznew)
          enddo
          sxnew=sxnew/slen
          synew=synew/slen
          sznew=sznew/slen

          dE= nsx*(sx(is)-sxnew) + nsy*(sy(is)-synew) + nsz*(sz(is)-sznew)
          if (dE<0.or.(exp(-dE*beta)>rkiss05())) then
             sx(is)=sxnew
             sy(is)=synew
             sz(is)=sznew
          endif
      endif     ! of if(occu(is))
      enddo     ! of do is ...


      end subroutine metro_sweep


      subroutine measurement
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! measures energy and magnetization
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      sweepen=0.D0
      mx=0.D0
      my=0.D0
      mz=0.D0

      do is=0,L3-1
         if (occu(is)) then
            mx=mx+sx(is)
            my=my+sy(is)
            mz=mz+sz(is)
            nsx=0.D0
            nsy=0.D0
            nsz=0.D0
            if (occu(m1(is))) then
               nsx=nsx+sx(m1(is))
               nsy=nsy+sy(m1(is))
               nsz=nsz+sz(m1(is))
            endif
            if (occu(m3(is))) then
               nsx=nsx+sx(m3(is))
               nsy=nsy+sy(m3(is))
               nsz=nsz+sz(m3(is))
            endif
            if (occu(m5(is))) then
               nsx=nsx+sx(m5(is))
               nsy=nsy+sy(m5(is))
               nsz=nsz+sz(m5(is))
            endif
            sweepen=sweepen-sx(is)*nsx
            sweepen=sweepen-sy(is)*nsy
            sweepen=sweepen-sz(is)*nsz
         endif
      enddo
      sweepen=sweepen/L3
      en=en+sweepen
      en2=en2+sweepen**2

      sweepmag=   sqrt(mx*mx+my*my+mz*mz)/L3
      mag = mag +  sweepmag
      mag2= mag2+  sweepmag**2
      mag4= mag4+  sweepmag**4

      enmag=enmag+ sweepen*sweepmag

      end subroutine measurement

      subroutine stiffness
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! measures stiffness in X,Y, and Z
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      ax  = 0.D0    ! vector a, perpendicular to the magnetic field
      ay  = 0.D0
      stx = 0.D0
      sty = 0.D0
      stz = 0.D0

      sweepstiffx = 0.D0
      sweepstiffy = 0.D0
      sweepstiffz = 0.D0

      ax=my/sqrt(mx*mx+my*my)
      ay=-mx/sqrt(mx*mx+my*my)

      beta = 1.D0/T

      do is=0, L3-1

          if (occu(is)) then
              if(occu(m1(is))) then                                                                                     ! m1 is the x-direction
                  sweepstiffx = sweepstiffx + (sx(is)*(sx(m1(is))-ax*(ax*sx(m1(is))+ay*sy(m1(is))))&
                                +sy(is)*sy(m1(is))-ay*(ax*sx(m1(is))+ay*sy(m1(is)))+sz(is)*(sz(m1(is))))                ! x-direction stiffness dot product, first part of our Hamiltonian
                  stx = stx + (-ax*(sy(is)*sz(m1(is))-sz(is)*sy(m1(is)))+ay*(sx(is)*sz(m1(is))-sz(is)*sx(m1(is))))      ! x-direction stiffness cross product, second part of our Hamiltonian
              endif
              if(occu(m3(is))) then                                                                                     ! m3 is the y-direction
                  sweepstiffy = sweepstiffy + (sx(is)*(sx(m3(is))-ax*(ax*sx(m3(is))+ay*sy(m3(is))))&
                                +sy(is)*sy(m3(is))-ay*(ax*sx(m3(is))+ay*sy(m3(is)))+sz(is)*(sz(m3(is))))                ! y-direction stiffness dot product, first part of our Hamiltonian
                  sty = sty + (-ax*(sy(is)*sz(m3(is))-sz(is)*sy(m3(is)))+ay*(sx(is)*sz(m3(is))-sz(is)*sx(m3(is))))      ! y-direction stiffness cross product, second part of our Hamiltonian
              endif
              if(occu(m5(is))) then                                                                                     ! m5 is the z-direction
                  sweepstiffz = sweepstiffz + (sx(is)*(sx(m5(is))-ax*(ax*sx(m5(is))+ay*sy(m5(is))))&
                                +sy(is)*sy(m5(is))-ay*(ax*sx(m5(is))+ay*sy(m5(is)))+sz(is)*(sz(m5(is))))                ! z-direction stiffness dot product, first part of our Hamiltonian
                  stz = stz + (-ax*(sy(is)*sz(m5(is))-sz(is)*sy(m5(is)))+ay*(sx(is)*sz(m5(is))-sz(is)*sx(m5(is))))      ! z-direction stiffness cross product, second part of our Hamiltonian
              endif
          endif
      enddo

      stiffx = stiffx + (sweepstiffx - beta*stx**2)/(L3)
      stiffy = stiffy + (sweepstiffy - beta*sty**2)/(L3)
      stiffz = stiffz + (sweepstiffz - beta*stz**2)/(L3)

      end subroutine stiffness

      subroutine corr_func
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! measures correlation function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      real(r8b)   :: Remq1x,Remq1y,Remq1z,Immq1x,Immq1y,Immq1z
      real(r8b)   :: Remq2x,Remq2y,Remq2z,Immq2x,Immq2y,Immq2z
      real(r8b)   :: Remq3x,Remq3y,Remq3z,Immq3x,Immq3y,Immq3z

      Remq1x=0.D0
      Remq1y=0.D0
      Remq1z=0.D0
      Immq1x=0.D0
      Immq1y=0.D0
      Immq1z=0.D0

      Remq2x=0.D0
      Remq2y=0.D0
      Remq2z=0.D0
      Immq2x=0.D0
      Immq2y=0.D0
      Immq2z=0.D0

      Remq3x=0.D0
      Remq3y=0.D0
      Remq3z=0.D0
      Immq3x=0.D0
      Immq3y=0.D0
      Immq3z=0.D0

      do i1=0, L-1
      do i2=0, L-1
      do i3=0, L-1
         is = L*L*i1 + L*i2 + i3
         if (occu(is)) then
            Remq1x=Remq1x+sx(is)*cos(qspace*i1)
            Remq1y=Remq1y+sy(is)*cos(qspace*i1)
            Remq1z=Remq1z+sz(is)*cos(qspace*i1)
            Immq1x=Immq1x+sx(is)*sin(qspace*i1)
            Immq1y=Immq1y+sy(is)*sin(qspace*i1)
            Immq1z=Immq1z+sz(is)*sin(qspace*i1)

            Remq2x=Remq2x+sx(is)*cos(qspace*i2)
            Remq2y=Remq2y+sy(is)*cos(qspace*i2)
            Remq2z=Remq2z+sz(is)*cos(qspace*i2)
            Immq2x=Immq2x+sx(is)*sin(qspace*i2)
            Immq2y=Immq2y+sy(is)*sin(qspace*i2)
            Immq2z=Immq2z+sz(is)*sin(qspace*i2)

            Remq3x=Remq3x+sx(is)*cos(qspace*i3)
            Remq3y=Remq3y+sy(is)*cos(qspace*i3)
            Remq3z=Remq3z+sz(is)*cos(qspace*i3)
            Immq3x=Immq3x+sx(is)*sin(qspace*i3)
            Immq3y=Immq3y+sy(is)*sin(qspace*i3)
            Immq3z=Immq3z+sz(is)*sin(qspace*i3)
         endif
      enddo
      enddo
      enddo
      sweepmagq1=(Remq1x**2+Remq1y**2+Remq1z**2 + Immq1x**2+Immq1y**2+Immq1z**2)
      sweepmagq2=(Remq2x**2+Remq2y**2+Remq2z**2 + Immq2x**2+Immq2y**2+Immq2z**2)
      sweepmagq3=(Remq3x**2+Remq3y**2+Remq3z**2 + Immq3x**2+Immq3y**2+Immq3z**2)
      sweepmagq1=sqrt(sweepmagq1)/L3
      sweepmagq2=sqrt(sweepmagq2)/L3
      sweepmagq3=sqrt(sweepmagq3)/L3
      magqs=magqs+(1.D0/3.D0)*(sweepmagq1+sweepmagq2+sweepmagq3)
      Gs=Gs+(1.D0/3.D0)*(sweepmagq1**2+sweepmagq2**2+sweepmagq3**2)                   !!!!! averaging of the fourier transform

      end subroutine corr_func

end !end contains

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123
!
!
! A   call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05   call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
!
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
!
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit
!        v0.93    Aug 13, 2012    changed integer representation test to avoid data statements
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      FUNCTION rkiss05()
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

     real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31

      real(r8b)             :: rkiss05
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w

      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1
      else
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         y=idum+1
      else
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1
      else
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1
      else
         w=idum
      endif

      rdum=rkiss05()

      return
      end subroutine kissinit
