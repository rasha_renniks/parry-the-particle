#!/bin/bash

comment="test"
jno="15557"

echo "Enter comment:"
read comment
echo "Job Number?:"
read jno
corre="fle3_$(date +"%m.%d.%Y_%H.%M.%S")-$comment"
echo $corre
mkdir $corre
mv *.dat $corre
mv "fle3.pbs.e$jno" $corre
mv "fle3.pbs.o$jno" $corre
cp *.plt $corre
cp corremos.f90 $corre
cp fle3.pbs $corre
