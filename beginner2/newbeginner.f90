program snickerdoodles
implicit none

integer,parameter                  :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter                  :: i4b= SELECTED_INT_KIND(8)
integer,parameter                  :: data_size = 5
integer(i4b)                       :: particle_num
integer(i4b)                       :: i, j,k
real(r8b)                          :: random_force_y
real(r8b)                          :: amplitude
real(r8b)                          :: fiction_force
real(r8b)                          :: initial_value
real(r8b)                          :: initial_velocity
integer(i4b)                       :: iterations
real(r8b)                          :: temp_particle, transdata(data_size), auxdata(data_size)
real(r8b)                          :: time_interval
real(r8b)                          :: average,deviation
real(r8b), allocatable             :: velocity(:), plarticle_path(:)
integer(i4b)                       :: seed
real(r8b)                          :: time
logical                            :: time_choice
real(r8b)                          :: rkiss05  

200 FORMAT(f6.2," ", 2f14.8)

!open files
open(unit = 2, file = "sample.txt")
open(unit = 3, file = "output.dat")

!initialize variables
fiction_force = 1.D0
print *, "Friction force coefficient set to 1"
print *, "Amplitude of Random Force:"
read (2,*) amplitude
print *, amplitude

print *, "initial value:"
read (2,*) initial_value
print *, initial_value

print *, "number of particles: "
read (2,*) particle_num
print *, particle_num

print *, "initial velocity: "
read (2,*) initial_velocity
print *, initial_velocity

print *, "total time: "
read (2,*) time
print *, time

print *, "total number of random forces to interact: "
read (2,*) iterations
print *, iterations
time_interval = time/iterations*1.D0

seed = 0
!TDSIZE = data_size

allocate(plarticle_path(particle_num))
allocate(velocity(particle_num))

!call MPI

!run each particle through walk
do i = 1, particle_num
  velocity(i) = initial_velocity
  plarticle_path(i) = initial_value
end do
k=0;
print *, "Time:  Average:    Deviation:"
do j = 0,(iterations)
  average = 0
  deviation = 0


    
    do i = 1,(particle_num)
      seed = k;
      call kissinit(seed)
      random_force_y = 2.D0*amplitude*rkiss05()-amplitude
      !print*, random_force_y
      plarticle_path(i) = plarticle_path(i) + velocity(i) * time_interval
      velocity(i) = velocity(i) + (-fiction_force*velocity(i)+random_force_y)*time_interval
      !calculate averages
      average = average + plarticle_path(i)      
      deviation = deviation + (plarticle_path(i)-initial_value)**2   
      k = k+1;
    end do
    
  average = average/particle_num
  deviation = deviation/particle_num
  print 200, time_interval*j, average, deviation
  write (3,200) time_interval*j, average, deviation
end do
 



!close files and deallocate
close(2)
close(3)
deallocate(plarticle_path,velocity) !this is where my problems are


!call MPI finalize
end program snickerdoodles

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis  
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123  
! 
! 
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
! 
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
! 
! 
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit 
!        v093     Aug 13, 2012    changed inter representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION rkiss05()
      implicit none
     
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31
 
      real(r8b)             :: rkiss05  
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w 
      
      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1 
      else 
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then 
         y=idum+1 
      else 
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1 
      else 
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1 
      else 
         w=idum
      endif

      rdum=rkiss05()
      
      return
      end subroutine kissinit