!test transdata
program test
 implicit none
 integer,parameter          :: i4b= SELECTED_INT_KIND(8)
 integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
 real(r8b)                  :: fun(0:3)
 integer(i4b)               :: iterations, x
print *, size(fun)
! !call test1()
! x=0
 ! do iterations = -57,57
   ! x=x+1
 ! end do

 ! print *, x
call test4()

end program test

SUBROUTINE test4()
 implicit none
 integer,parameter          :: i4b= SELECTED_INT_KIND(8)
 integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
 real(r8b)                  :: boxed,no_box, dt, maxi, test, summ
 integer(i4b)               :: i,j,k
 character                  :: nonsense(4)

dt=0.D0
open(unit=1, file="output.dat")
open(unit=2, file="outputb.dat")
open(unit=3, file="test.dat")

! read (1,*) nonsense
! read (2,*) nonsense

summ = 0.D0

do i=0,220
  
  dt = dt+0.01d0
  read (1,*) no_box
  read (2,*) boxed
  
  test = abs((boxed-no_box)/no_box)
  
  write (3,*) dt, test
  if (test>maxi) maxi = test
  
  if (test>0) summ = summ + test

end do

print *, "  max err               av error"
print *, maxi, summ/220
  
end SUBROUTINE test4



SUBROUTINE test3()
 implicit none
 integer,parameter          :: i4b= SELECTED_INT_KIND(8)
 integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
real(r8b), allocatable     :: KERNEL(:), g_forces(:), kernel2(:)
real(r8b), allocatable     :: boxed_kernel(:), box_velocity(:)
real(r8b)                  :: box_start, alpha, kernel_const2, time_interval
integer(i4b)               :: new_box, iterations, iteration2
integer(i4b)               :: i, j,k,l, x,m,h,s,box_size,iteration_power

iteration_power=8
iteration2 = 2**iteration_power
time_interval=0.01
alpha=1.5D0
box_size = 10
box_start = 100.D0
allocate(kernel2(0:iteration2+1))
allocate(KERNEL(0:iteration2))
allocate(boxed_kernel(0:(iteration2/box_size + MOD(iteration2,box_size))))
allocate(box_velocity(0:(iteration2/box_size + MOD(iteration2,box_size))))

do i=0,iteration2+1
  kernel2(i) = (1.D0*i)**(alpha) !FF2
end do

kernel_const2 = (time_interval**(alpha-1.D0))/( (alpha)*(alpha-1.D0) )

do k=0, iteration2 !FF2
  KERNEL(k) = kernel2(abs(k-1))-2.D0*kernel2(k)+ kernel2(abs(k+1))
  KERNEL(k) = kernel_const2*KERNEL(k)
end do

boxed_kernel(0)=KERNEL(iteration2)
i=0
j=1
do k=iteration2-1,0,-1
  if (j<=iteration2/10) then
    i=i+1
    if(i==1) boxed_kernel(j)=0.D0
    boxed_kernel(j) = boxed_kernel(j) + KERNEL(k)
    if(i==box_size) then
      i=0
      boxed_kernel(j)=boxed_kernel(j)/(1.D0*box_size)
      print *, k, j, KERNEL(k), boxed_kernel(j)
      j=j+1
      print *, ""
    else
      print *, k, j, KERNEL(k), boxed_kernel(j)
    end if
  else
    boxed_kernel(j)=KERNEL(k)
    print *, k, j, KERNEL(k), boxed_kernel(j)
    j=j+1
  end if
end do

deallocate(kernel2,KERNEL)
deallocate(box_velocity,boxed_kernel)

end SUBROUTINE test3




SUBROUTINE test2( iterations )
implicit none

integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
real(r8b), allocatable     :: average(:),deviation(:),aux_array(:)
real(r8b), allocatable     :: v_deviation(:)
real(r8b), dimension(:,:), allocatable     :: butt
integer(i4b)               :: iterations, i, datasize, k
    
    
allocate(average(0:iterations))
allocate(deviation(0:iterations))
allocate(v_deviation(0:iterations))
allocate(aux_array(0:iterations))

do i = 0, iterations
  average(i) = 0.D0
  deviation(i) = 0.D0
  v_deviation(i) = 0.D0
  aux_array(i) = i
end do


    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!mr chunky p1!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
     if (iterations+1>=10) then
       datasize = (iterations+MOD(iterations,2))/2
       allocate(butt(3,iterations+MOD(iterations,2)-datasize:iterations))
      do i = iterations+MOD(iterations,2)-datasize, iterations
        butt(1,i) = average(i)
        butt(2,i) = deviation(i)
        butt(3,i) = v_deviation(i)
      end do
     else 
       print *, "No Mr chunky"
       datasize = iterations+1
     end if
    
    !average
        do k = 0, datasize-1
          average(k) = average(k) + aux_array(k)
        end do
    
    !deviation
        do k = 0, datasize-1
          deviation(k) = deviation(k) + aux_array(k)
        end do
    
    !v_deviation
        depack: do k = 0, datasize-1
          v_deviation(k) = v_deviation(k) + aux_array(k)
        end do depack
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!!!mr chunky p2!!!!!!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    if (iterations+1>=10) then
      deallocate(aux_array)
      !print *, "mr chunky p2 ", iterations+MOD(iterations,2)-datasize
      allocate(aux_array(iterations+MOD(iterations,2)-datasize:iterations))
      print *, "size ", size(aux_array)
      print *, "test ", datasize-MOD(iterations,2)+1
      print *, "-"
      do i = iterations+MOD(iterations,2)-datasize, iterations
        aux_array(i) = i
      end do
      do i = 1,3
            do k = iterations+MOD(iterations,2)-datasize, iterations
              if (i==1) average(k) = average(k) + aux_array(k)
              if (i==2) deviation(k) = deviation(k) + aux_array(k)
              if (i==3) v_deviation(k) = v_deviation(k) + aux_array(k)
            end do
      end do
      deallocate(butt)
    end if
    
    ! do i = 0, iterations
      ! print *, average(i), deviation(i), v_deviation(i)
    ! end do
    
deallocate(average,deviation,v_deviation,aux_array)
    
end SUBROUTINE test2




SUBROUTINE test1()
implicit none

integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
integer(i4b)               :: iterations, TDSIZE, k
real(r8b),allocatable      :: transdata(:)

iterations = 15
TDSIZE = 2*iterations+3
allocate(transdata(0:TDSIZE))

package: do k=0,iterations
  transdata(k) = k
  transdata(k+iterations+1) = k
end do package
transdata(TDSIZE-1) = 16
transdata(TDSIZE) = 17

do k=0, TDSIZE
  print *, transdata(k)
end do

deallocate(transdata)

end SUBROUTINE test1