program taternater
implicit none

integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)
real, allocatable, dimension(:) :: particle, particle_average, particle_deviation
integer particle_num
integer i
real(r8b) random_force_y
real amplitude
real fiction_force
real(r8b) initial_value
real(r8b) initial_velocity
real(r8b) velocity
integer iterationsn !transdata(1) = final particle positions, transdata(2) average particle position
real(r8b) :: temp_particle, transdata(2), auxdata(2)
real time_interval
real(r8b) :: final_averages, average_average, average
real(r8b) deviation
integer :: seed, TDSIZE, status(MPI_STATUS_SIZE)
real time, ierr

include 'mpif.h'

!initialize variables
fiction_force = 1.0
print *, "Friction force coefficient set to 1"
print *, "Enter Amplitude of Random Force:"
read *, amplitude
print *, "Enter initial value:"
read *, initial_value
print *, "Enter number of particles: "
read*, particle_num
print *, "Enter initial velocity: "
read *, initial_velocity
print *, "Enter total time: "
read *, time
print *, "Enter total number of random forces to interact"
print *, "with particle or time intervals of each interaction"
print *, "----"
print *, "0. Total Number"
print *, "1. Time interval"
read *, time_choice
if (time_choice==0)
  print *, "Enter total number: "
  read *, iterations
  time_interval = time/iterations*1.0
else
  print *, "Enter Time Interval: "
  read *, time_interval
  iterations = time/time_interval
end if

average = 0
deviation = 0
seed = 0
TDSIZE = 1
open(unit = 1, file = "output.txt")

amplitude = amplitude*1.0
allocate(particle(particle_num))
allocate(particle_average(particle_num))
allocate(particle_deviation(particle_num))

call MPI_INIT(ierr)
call MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
call MPI_COMM_SIZE(MPI_COMM_WORLD, numprocs, ierr)

!run each particle through a walk
do i = myid+1,particle_num, numprocs
  call walk(temp_particle, amplitude, initial_value, iterations, fiction_force, initial_velocity, time_interval, seed, average)
  transdata(1) = temp_particle
  transdata(2) = average
  seed = seed + 1
  if (myid.ne.0) then                                    !zero for host node
    call MPI_SEND(transdata,TDSIZE,MPI_DOUBLE_PRECISION,0, myid, MPI_COMM_WORLD, ierr)
  else
    do id = 0, numprocs-1
      if (id==0) then
        auxdata(:)=transdata(:)
      else
        call MPI_RECV(auxdata,TDSIZE,MPI_DOUBLE_PRECISION,id,id,MPI_COMM_WORLD,status,ierr)
      end if
      final_averages = final_averages + transdata(1)
      write(1,*) transdata(1)  !final particle positions
      deviation = deviation + (transdata(1)-transdata(2))**2 
    end do
  end if
end do

MPI_FINALIZE()

final_averages = final_averages/particle_num
deviation = deviation/particle_num

  print *, "Average: ", final_averages, "  deviation: ", deviation

close(1)
deallocate(particle)
end program taternater

!---------------------------------------------------------------------------------

subroutine walk(particle, amplitude, initial_value, iterations, fiction_force, initial_velocity, time_interval, seed, average)
implicit none
integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)
real(r8b) particle
real amplitude
real fiction_force
real(r8b) initial_value
real(r8b) velocity, average
integer iterations
real(r8b) initial_velocity
integer i
real(r8b)             :: rkiss05  
real(r8b) random_force_y
real time_interval
integer seed
average = 0
particle = initial_value
velocity = initial_velocity
do i = 0, iterations
  seed = seed + i
  call kissinit(seed)
  random_force_y = 2.0*amplitude*rkiss05()-amplitude
  
  particle = particle + velocity * time_interval
  velocity = velocity + (-fiction_force*velocity+random_force_y)*time_interval
  average = average + particle
  
end do
average = average/iterations

end subroutine walk

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis  
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123  
! 
! 
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
! 
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
! 
! 
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit 
!        v093     Aug 13, 2012    changed inter representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION rkiss05()
      implicit none
     
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31
 
      real(r8b)             :: rkiss05  
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w 
      
      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1 
      else 
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then 
         y=idum+1 
      else 
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1 
      else 
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1 
      else 
         w=idum
      endif

      rdum=rkiss05()
      
      return
      end subroutine kissinit