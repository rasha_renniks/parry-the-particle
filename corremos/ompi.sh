#!/bin/bash

comment="test"
jno="15557"

echo "Enter comment:"
read comment
echo "Job Number?:"
read jno
corre="mpicorremos$(date +"%m.%d.%Y_%H.%M.%S")-$comment"
echo $corre
mkdir $corre
mv *.dat $corre
mv "toodles.pbs.e$jno" $corre
mv "toodles.pbs.o$jno" $corre
cp *.plt $corre
cp corremos.f90 $corre
cp toodles.pbs $corre
