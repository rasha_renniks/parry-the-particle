!--------------------------------------------------
!Author: Sarah Skinner     Instructor: Thomas Vojta
!Program: Corremos.f95     Project: Parry the Particle
!Last Edited: 11.3.2018
!--------------------------------------------------
!Program Description: This program generates the 
!    average, deviation, and histogram of the 
!    simulation of particles in a one-dimensional
!    space with a specified number of random forces 
!    interacting on each particle within a
!    specified time. The random numbers generated
!    are correlated with a gaussian probability
!    density.
!Program Notes:  1<alpha<2;
!
  !If you set M = 0 in input file sample.txt
  !then the program will automatically calc
  !an M so that total time will be greater than
  !total user defined time;
  !
  !Program is set up to be run in MPI or on normal 
  !depending on whether or not you enable #define PARALLEL
  !When not using PARALLEL, compile with Intel fortran compiler (ifort)
  !When using PARALLEL, compile with Open MPI (mpifort)
!--------------------------------------------------


program corremos
#define version 'corremos'
#define PARALLEL
#define BOXED

USE FFT
implicit none

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!Variable Declarations!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
logical, parameter         :: optimization_file = .false.

                              !histograms
integer,parameter          :: histosize = 200
                              !total histo      histo near wall        histo of g_kiss05/corvec
real(r8b)                  :: histo(histosize)!,! fine_histo(histosize) !, g_rand(2,histosize)
real(r8b)                  :: histo12(histosize)!,! fine_histo12(histosize) !, g_rand(2,histosize)
real(r8b)                  :: histo15(histosize)!,! fine_histo15(histosize) !, g_rand(2,histosize)
real(r8b)                  :: histo18(histosize)!,! fine_histo18(histosize) !, g_rand(2,histosize)
real(r8b)                  :: histo21(histosize)!,! fine_histo21(histosize) !, g_rand(2,histosize)
real(r8b)                  :: histo_clean(histosize)!,! fine_histo_clean(histosize) !, g_rand(2,histosize)
integer(i4b),parameter     :: cleanup_term = 10
real(r8b),parameter        :: histo_final_spread=30.D0 ![-A,A]
real(r8b)                  :: x_clean, temp2, sigma, sigma12, sigma15, sigma18, sigma21


                              !user defined system parameters
integer(i4b)               :: particle_num, iteration_power, iterations, iteration2
real(r8b)                  :: temperature, alpha,  time_interval
real(r8b)                  :: initial_value, initial_velocity, time
real(r8b)                  :: F0, lambda !for wall force

                              !ongoing system variables
real(r8b)                  :: random_force_y, wall_force, friction_sum
real(r8b)                  :: fiction_force, gforce_const
real(r8b), allocatable     :: plarticle_path(:), velocity(:)

                              !output variables into parameters.dat
real(r8b)                  :: maxpos = 0.D0, minpos = 0.D0

                              !variables for intermediatary stops
real(r8b)                  :: temp_particle
real(r8b)                  :: average_temp,deviation_temp,v_deviation_temp
integer(i4b)               :: stop1, stop2, stop3, wall_choice, temp_particle_num

                              !stops for output files at certain intervals
character(50)              :: c !for naming file at certain intervals
character(50)              :: filename = 'output'
character(4)               :: ending = '.dat', nonsense !ver imp.

                              !unimportant iteration variables
integer(i4b)               :: i, j,k,l, x,n,h,s, g, M, iskip
integer(i4b),parameter     :: NSKIP = 25
real(r8b)                  :: temp

                              !final output arrays for output.dat
real(r8b), allocatable     :: average(:),deviation(:)
real(r8b), allocatable     :: v_deviation(:)

                              !important constants for calculations
real(r8b), allocatable     :: KERNEL(:), g_forces(:), kernel2(:)
real(r8b)                  :: kernel_const2

#ifdef BOXED
integer(i4b)               :: box_dens(4), box_window_size(4), ndif, half_window
integer(i4b)               :: odd_factor2, odd_factor3, odd_factor4
real(r8b), allocatable     :: velo_appr_1(:,:), velo_appr_2(:,:), velo_appr_3(:,:), velo_appr_4(:,:)
real(r8b), allocatable     :: dkernel(:), ddkernel(:)
integer(i4b) :: i1,i2,i3,i4,d1,d2,d3,d4  !approximation indexing/integration helpers
#endif

                              !All the MPI Stuff
#ifdef PARALLEL
  include 'mpif.h'
  integer(i4b)               :: status(MPI_STATUS_SIZE), id, TDSIZE
  integer(i4b)               :: myid,numprocs,namesize, ierr, errcode, datasize
  real(r8b),allocatable      :: transdata(:), auxdata(:), aux_array(:)
  real(r8b),allocatable      :: t_average(:),t_deviation(:),t_v_deviation(:)
  real(r8b)                  :: big_min, big_max, t_histo(histosize), t_fhisto(histosize)
  real(r8b)                  :: t_histo12(histosize)!, !t_fhisto12(histosize)
  real(r8b)                  :: t_histo15(histosize)!, !t_fhisto15(histosize)
  real(r8b)                  :: t_histo18(histosize)!, !t_fhisto18(histosize)
  real(r8b)                  :: t_histo21(histosize)!, !t_fhisto21(histosize)
  real(r8b)                  :: aux_histo(histosize), t_histo_clean(histosize)!, t_fhisto_clean(histosize)
  big_min = 0.D0
  big_max = 0.D0
#else
  real :: start, finish
  call cpu_time(start)
#endif

!--------------------------------------------------------------

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!Set up!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!output formatting
200 FORMAT(" ",f12.3," ", 33f20.8)
201 FORMAT(" ",a9," ", 33a20)

!open files
open(unit = 2, file = "sample.txt")
open(unit = 4, file = "parameters.dat")

!initialize variables, output to parameters.dat
fiction_force = 1.D0

write (4,*) "Friction force coefficient set to 1"
write (4,*) "Mass is set to one."
read (2,*) nonsense, temperature
write (4,*) "Temperature:", temperature
read (2,*) nonsense, initial_value
write (4,*) "initial value:", initial_value
read (2,*) nonsense, initial_velocity
write (4,*) "initial velocity: ", initial_velocity
read (2,*) nonsense, particle_num
write (4,*) "number of particles set to: ", particle_num

stop1 = particle_num/4
stop2 = particle_num/2
stop3 = (3*particle_num)/4

read (2,*) nonsense, time_interval
write (4,*) "time interval: ", time_interval
read (2,*) nonsense, iteration_power !power for 2
read (2,*) nonsense, time

if (iteration_power.eq.0) then
  iteration_power = M( time_interval, time)
  !print *, iteration_power
  iterations = time/time_interval+1
  iteration2 = 2**iteration_power
else
  iteration2 = 2**iteration_power
  !print *, iteration2
  iterations = iteration2
  time = time_interval*iterations
end if
write (4,*) "total time: ", time
write (4,*) "total number of random forces to interact: ", iterations
read (2,*) nonsense, alpha
write (4,*) "Alpha for friction force set to ", alpha
write (4,*) "Stops:",stop1, stop2,stop3
read (2,*) nonsense, lambda
write(4,*) "Alpha for wall force set to ", lambda
read (2,*) nonsense, F0
write(4,*) "Constant in front of wall force set to ", F0
close(2)

!allocate arrays
allocate(plarticle_path(0:iterations))
allocate(velocity(0:iterations))
allocate(average(0:iterations))
allocate(deviation(0:iterations))
allocate(v_deviation(0:iterations))
allocate(kernel2(0:iteration2+1))
allocate(KERNEL(0:iteration2))
allocate(g_forces(0:iteration2-1))

#ifdef PARALLEL
open(unit = 1, file = "visual_check.dat")
TDSIZE = 2
allocate(transdata(TDSIZE))
allocate(auxdata(TDSIZE))
allocate(aux_array(0:iterations))
allocate(t_average(0:iterations))
allocate(t_deviation(0:iterations))
allocate(t_v_deviation(0:iterations))
#endif

call initialize

#ifdef BOXED
  call initialize_box
#endif














!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!Set up MPI!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef PARALLEL
  call MPI_INIT(ierr)
  call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
  call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )
  if(myid==0) write (4,*) "Program running on", numprocs, " processes"
#else
  write (4,*) "Program running on a single processor"
#endif
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!walk each particle!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!
iskip = 0

#ifdef PARALLEL
  if (myid==0) write (4,*) "Real number of particles: ", (particle_num/numprocs)*numprocs
  particles: do j = myid+1, (particle_num/numprocs)*numprocs, numprocs
#else
  particles: do j = 1,particle_num
  print *, "running..."
#endif
    iskip=iskip+1
    print *, iskip, "particle:", j
    plarticle_path(0) = initial_value
    velocity(0) = initial_velocity
    x_clean = 0
    !!!!!!!!!!!!!!!!!!!!!!!!!!!
    !make forces for all runs!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!
    call gkissinit(j+40000)
    call corvec( g_forces, iteration2, iteration_power, alpha)

#ifdef BOXED
    i1 = 0
    i2 = 0
    i3 = 0
    i4 = 0
    d1 = -box_window_size(1)/2
    d2 = -box_window_size(2)/2
    d3 = -box_window_size(3)/2
    d4 = -box_window_size(4)/2
#endif
    !!!!!!!!!!!!!!!!!!!!!!!!!
    !individual particle run!
    !!!!!!!!!!!!!!!!!!!!!!!!!
    !open(unit=12,file = "checkup.dat")
    run: do i = 1,iterations
      
      
      !calc new force for next iteration
      random_force_y = gforce_const*(g_forces(i-1))
      
      !move particle
      plarticle_path(i) = plarticle_path(i-1) + velocity(i-1) * time_interval
      
      !physical wall
      wall_force = F0*exp(-1.D0*plarticle_path(i)*lambda)
      
      !Friction force
#ifdef BOXED
      call approx_velos
      call ff_approx
#else
      call ff
#endif
      
      !calculate new velocity for next iteration
      velocity(i) = velocity(i-1) + ( -friction_sum + random_force_y + wall_force)*time_interval
      !print *, i, plarticle_path(i), friction_sum
     
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      !add to averages and deviation!
      !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      if( abs(plarticle_path(i))<10000.D0) then
        average(i) = average(i) + plarticle_path(i)
        deviation(i) = deviation(i) + (plarticle_path(i)-initial_value)**2
        v_deviation(i) = v_deviation(i) + (velocity(i)-initial_velocity)**2
        if (plarticle_path(i)>maxpos) then
          maxpos = plarticle_path(i)
        end if
        if (plarticle_path(i)<minpos) then
          minpos = plarticle_path(i)
        end if
      else
        print *, "Something went wrong", i
        call exit()
      end if
      
      
        if ( (i-1)==2**12 ) then
          x = (plarticle_path(i-1)*(100.D0/(histo_final_spread)))
          if ((x<100).and.(x>-100)) then
            histo12(x+100) = histo12(x+100) + 1
          end if
          x = (plarticle_path(i-1)*50.D0)
          ! if ((x<100).and.(x>-100)) then
            ! fine_histo12(x+100) = fine_histo12(x+100) + 1
          ! end if
        end if
      
        if ( (i-1)==2**15 ) then
          x = (plarticle_path(i-1)*(100.D0/(histo_final_spread)))
          if ((x<100).and.(x>-100)) then
            histo15(x+100) = histo15(x+100) + 1
          end if
          x = (plarticle_path(i-1)*50.D0)
          ! if ((x<100).and.(x>-100)) then
            ! fine_histo15(x+100) = fine_histo15(x+100) + 1
          ! end if
        end if
        
        if ( (i-1)==2**18 ) then
          if (optimization_file) print *, "2 percent"
          x = (plarticle_path(i-1)*(100.D0/(histo_final_spread)))
          if ((x<100).and.(x>-100)) then
            histo18(x+100) = histo18(x+100) + 1
          end if
          x = (plarticle_path(i-1)*50.D0)
          ! if ((x<100).and.(x>-100)) then
            ! fine_histo18(x+100) = fine_histo18(x+100) + 1
          ! end if
        end if
        
        if ( (i-1)==2**21 ) then
          if (optimization_file) print *, "12 percent"
         x = (plarticle_path(i-1)*(100.D0/(histo_final_spread)))
          if ((x<100).and.(x>-100)) then
            histo21(x+100) = histo21(x+100) + 1
          end if
          x = (plarticle_path(i-1)*50.D0)
          ! if ((x<100).and.(x>-100)) then
            ! fine_histo21(x+100) = fine_histo21(x+100) + 1
          ! end if
        end if
        
        if ( (i-1).ge.(iterations-cleanup_term) ) then
          x_clean = x_clean + (plarticle_path(i-1)*(100.D0/(histo_final_spread)))
        end if
        !if (MOD(i,100)==0) write (12,*) i, plarticle_path(i)
      
    end do run
    
    
    !close(12)
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !add final position to histogram!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    x = (plarticle_path(iterations-1)*(100.D0/(histo_final_spread)))
    if ((x<100).and.(x>-100)) then
      histo(x+100) = histo(x+100) + 1
    end if
    x = (plarticle_path(iterations-1)*50.D0)
    ! if ((x<100).and.(x>-100)) then
      ! fine_histo(x+100) = fine_histo(x+100) + 1
    ! end if
    
    x_clean = x_clean/(1.D0*cleanup_term)
    if ((x_clean<100).and.(x_clean>-100)) then
      histo_clean(x_clean+100) = histo_clean(x_clean+100) + 1
    end if

    
#ifdef PARALLEL
    
    if (myid==0) write(1,*) "particle:",j,minpos,maxpos
    
    if( (iskip/NSKIP)*NSKIP.eq.iskip) then   
      
      if (myid==0) then
        t_histo(:)=histo(:)
        !t_fhisto(:)=fine_histo(:)
        t_histo12(:)=histo12(:)
        !t_fhisto12(:)=fine_histo12(:)
        t_histo15(:)=histo15(:)
        !t_fhisto15(:)=fine_histo15(:)
        t_histo18(:)=histo18(:)
        !t_fhisto18(:)=fine_histo18(:)
        t_histo21(:)=histo21(:)
        !t_fhisto21(:)=fine_histo21(:)
        t_histo_clean(:)=histo_clean(:)
        !t_fhisto_clean(:)=fine_histo_clean(:)
        t_average(:)=average(:)
        t_deviation(:)=deviation(:)
        t_v_deviation(:)=v_deviation(:)
      end if
      !if (myid==0) write (1,*) "begin sends"
                    
                    datasize = iterations+1
                    
                    if (myid.ne.0) then
                      call MPI_SEND(histo,histosize,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo,histosize,MPI_DOUBLE_PRECISION,0,2,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(average,datasize,MPI_DOUBLE_PRECISION,0,3,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(deviation,datasize,MPI_DOUBLE_PRECISION,0,4,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(v_deviation,datasize,MPI_DOUBLE_PRECISION,0,5,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(histo12,histosize,MPI_DOUBLE_PRECISION,0,6,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo12,histosize,MPI_DOUBLE_PRECISION,0,7,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(histo15,histosize,MPI_DOUBLE_PRECISION,0,8,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo15,histosize,MPI_DOUBLE_PRECISION,0,9,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(histo18,histosize,MPI_DOUBLE_PRECISION,0,10,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo18,histosize,MPI_DOUBLE_PRECISION,0,11,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(histo21,histosize,MPI_DOUBLE_PRECISION,0,12,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo21,histosize,MPI_DOUBLE_PRECISION,0,13,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(histo_clean,histosize,MPI_DOUBLE_PRECISION,0,13,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo_clean,histosize,MPI_DOUBLE_PRECISION,0,15,MPI_COMM_WORLD,ierr)
                    else
                      do id=1, numprocs-1
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,1,MPI_COMM_WORLD,status,ierr)
                        histo(:) = histo(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,2,MPI_COMM_WORLD,status,ierr)
                        !fine_histo(:) = fine_histo(:) + aux_histo(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,3,MPI_COMM_WORLD,status,ierr)
                        average(:) = average(:) + aux_array(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,4,MPI_COMM_WORLD,status,ierr)
                        deviation(:) = deviation(:) + aux_array(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,5,MPI_COMM_WORLD,status,ierr)
                        v_deviation(:) = v_deviation(:) + aux_array(:)
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,6,MPI_COMM_WORLD,status,ierr)
                        histo12(:) = histo12(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,7,MPI_COMM_WORLD,status,ierr)
                        !fine_histo12(:) = fine_histo12(:) + aux_histo(:)
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,8,MPI_COMM_WORLD,status,ierr)
                        histo15(:) = histo15(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,9,MPI_COMM_WORLD,status,ierr)
                        !fine_histo15(:) = fine_histo15(:) + aux_histo(:)
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,10,MPI_COMM_WORLD,status,ierr)
                        histo18(:) = histo18(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,11,MPI_COMM_WORLD,status,ierr)
                        !fine_histo18(:) = fine_histo18(:) + aux_histo(:)
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,12,MPI_COMM_WORLD,status,ierr)
                        histo21(:) = histo21(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,13,MPI_COMM_WORLD,status,ierr)
                        !fine_histo21(:) = fine_histo21(:) + aux_histo(:)
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,13,MPI_COMM_WORLD,status,ierr)
                        histo_clean(:) = histo_clean(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,15,MPI_COMM_WORLD,status,ierr)
                        !fine_histo_clean(:) = fine_histo_clean(:) + aux_histo(:)
                      end do
                    end if
                    
    
    !intermediatary output files in case of crash
    if ((myid==0)) then
      temp_particle_num=j+numprocs
      write (1,*) "start output"
#else
    !for visual checks on single processor
    print *, "particle:",j,minpos,maxpos
    if (MOD(j,1)==0) then
      temp_particle_num = j
      stop1=stop2
      stop2=stop3
      stop3=particle_num+1
#endif
      
 
        filename = 'output'
        !write( c, '(i0)' ) i*time_interval
        filename = trim(filename)// ending
        open(unit = 9, file = trim(filename), status = 'replace')
        if(optimization_file) open(unit = 10, file = 'outputb.dat', status = 'replace')
        rewind(9)
        write (9,*) temp_particle_num, " out of ", particle_num
        write (9,201) "dt", "<x>", "<x^2>", "<v^2>"
        do i = 0,iterations,100 !outputs every second
          average_temp = average(i)/temp_particle_num
          deviation_temp = deviation(i)/temp_particle_num
          v_deviation_temp = v_deviation(i)/temp_particle_num
          write (9,200) time_interval*i, average_temp, deviation_temp, v_deviation_temp
          if(optimization_file) write (10,*) deviation_temp
        end do
      
        close(9)
        if(optimization_file) close(10)
        filename = 'histos'
        filename = trim(filename)// ending
        open(unit = 9, file = trim(filename),status = 'replace')
        rewind(9)
        filename = 'histos12'
        filename = trim(filename) // ending
        open(unit = 10, file = trim(filename),status = 'replace')
        rewind(10)
        filename = 'histos15'
        filename = trim(filename) // ending
        open(unit = 11, file = trim(filename),status = 'replace')
        rewind(11)
        filename = 'histos18'
        filename = trim(filename)// ending
        open(unit = 12, file = trim(filename),status = 'replace')
        rewind(12)
        filename = 'histos21'
        filename = trim(filename) // ending
        open(unit = 13, file = trim(filename),status = 'replace')
        rewind(13)
        filename = 'histos_clean'
        filename = trim(filename) // ending
        open(unit = 14, file = trim(filename),status = 'replace')
        rewind(14)
        
        write(9,201) "x", "P(x)","x/sigma","P*sigma"
        write(10,201) "x", "P(x)","x/sigma","P*sigma"
        write(11,201) "x", "P(x)","x/sigma","P*sigma"
        write(12,201) "x","P(x)","x/sigma","P*sigma"
        write(13,201) "x", "P(x)","x/sigma","P*sigma"
        write(14,201) "x", "P(x)","x/sigma","P*sigma"
        
        temp2 = (100.D0/(histo_final_spread))/(1.D0*temp_particle_num)
        sigma = sqrt(v_deviation(iterations-1)/temp_particle_num)
        sigma12 = sqrt(v_deviation((2**12)-1)/temp_particle_num)
        sigma15 = sqrt(v_deviation((2**15)-1)/temp_particle_num)
        sigma18 = sqrt(v_deviation((2**18)-1)/temp_particle_num)
        sigma21 = sqrt(v_deviation((2**21)-1)/temp_particle_num)
        do l = 1, histosize
          temp = (l-100)/(100.D0/(histo_final_spread))
          write (9,200) temp, histo(l)*temp2, temp/sigma, histo(l)*temp2*sigma
          write (14,200) temp, histo_clean(l)*temp2, temp/sigma, histo_clean(l)*temp2*sigma
          write (10,200) temp, histo12(l)*temp2, temp/sigma12, histo12(l)*temp2*sigma12 
          write (11,200) temp, histo15(l)*temp2, temp/sigma15, histo15(l)*temp2*sigma15
          write (12,200) temp, histo18(l)*temp2, temp/sigma18, histo18(l)*temp2*sigma18 
          write (13,200) temp, histo21(l)*temp2, temp/sigma21, histo21(l)*temp2*sigma21
        end do
        close(9)
        close(10)
        close(11)
        close(12)
        close(13)
        close(14)

#ifdef PARALLEL
      histo(:)=t_histo(:)
      !fine_histo(:)=t_fhisto(:)
      histo12(:)=t_histo12(:)
      !fine_histo12(:)=t_fhisto12(:)
      histo15(:)=t_histo15(:)
      !fine_histo15(:)=t_fhisto15(:)
      histo18(:)=t_histo18(:)
      !fine_histo18(:)=t_fhisto18(:)
      histo21(:)=t_histo21(:)
      !fine_histo21(:)=t_fhisto21(:)
      histo_clean(:)=t_histo_clean(:)
      !fine_histo_clean(:)=t_fhisto_clean(:)
      average(:)=t_average(:)
      deviation(:)=t_deviation(:)
      v_deviation(:)=t_v_deviation(:)
      end if !myid ==0
#endif
        
    end if !NSKIP/stop
    
  end do particles

  
  
  
  
  
  
  
  
  
#ifdef PARALLEL
  
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!!!!package data!!!!!!!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    transdata(1) = minpos
    transdata(2) = maxpos
    
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    !!!!send or recieve data!!!!!
    !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    !transdata
    if (myid.ne.0) then
      call MPI_SEND(transdata,2,MPI_DOUBLE_PRECISION,0,myid,MPI_COMM_WORLD,ierr)
    else
      do id=0, numprocs-1
        if (id==0) then
           auxdata(:)=transdata(:)
        else
           call MPI_RECV(auxdata,2,MPI_DOUBLE_PRECISION,id,id,MPI_COMM_WORLD,status,ierr)
        endif
        !unpack data
        if (auxdata(1)<big_min) big_min = auxdata(1)
        if (auxdata(2)>big_max) big_max = auxdata(2)
      end do
    end if
    
                    !histo
                    if (myid.ne.0) then
                      call MPI_SEND(histo,histosize,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
                      !call MPI_SEND(fine_histo,histosize,MPI_DOUBLE_PRECISION,0,2,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(average,datasize,MPI_DOUBLE_PRECISION,0,3,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(deviation,datasize,MPI_DOUBLE_PRECISION,0,4,MPI_COMM_WORLD,ierr)
                      call MPI_SEND(v_deviation,datasize,MPI_DOUBLE_PRECISION,0,5,MPI_COMM_WORLD,ierr)
                    else
                      do id=1, numprocs-1
                        call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,1,MPI_COMM_WORLD,status,ierr)
                        histo(:) = histo(:) + aux_histo(:)
                        !call MPI_RECV(aux_histo,histosize,MPI_DOUBLE_PRECISION,id,2,MPI_COMM_WORLD,status,ierr)
                        !fine_histo(:) = fine_histo(:) + aux_histo(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,3,MPI_COMM_WORLD,status,ierr)
                        average(:) = average(:) + aux_array(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,4,MPI_COMM_WORLD,status,ierr)
                        deviation(:) = deviation(:) + aux_array(:)
                        call MPI_RECV(aux_array,datasize,MPI_DOUBLE_PRECISION,id,5,MPI_COMM_WORLD,status,ierr)
                        v_deviation(:) = v_deviation(:) + aux_array(:)
                      end do
                    end if
      
        

if (myid==0) then
#endif

open(unit = 3, file = "output.dat")
open(unit = 5, file = "histos.dat")
!unit = 6 is bad luck
open(unit = 8, file = "outputb.dat")

write (3,201) "dt", "<x>", "<x^2>", "<v^2>"
do i = 0,iterations,100 !outputs every second
  average(i) = average(i)/particle_num
  deviation(i) = deviation(i)/particle_num
  v_deviation(i) = v_deviation(i)/particle_num
  write (3,200) time_interval*i, average(i), deviation(i), v_deviation(i)
  write (8,*) deviation(i)
end do

temp2 = (100.D0/(histo_final_spread))/(1.D0*temp_particle_num)
sigma = sqrt(v_deviation(iterations-1)/temp_particle_num)
write(5,201) "dx", "prob_dens","dx2","prob_dens2"
do l = 1, histosize
  temp = (l-100)/(100.D0/(histo_final_spread))
  write (5,200) temp, histo(l)*temp2, temp/sigma, histo(l)*temp2*sigma
end do


#ifdef PARALLEL

write (4,*) "Maximum position:", big_max
write (4,*) "Minimum position:", big_min
end if

call MPI_FINALIZE(ierr)

#else
write (4,*) "Maximum position:", maxpos
write (4,*) "Minimum position:", minpos
#endif


#ifdef PARALLEL
  deallocate(transdata,auxdata,aux_array)
  close(1)
  deallocate(t_average,t_deviation,t_v_deviation)
#else
!just single processor only
  call cpu_time(finish)
  print *, "Runtime: ", finish-start
  write (4,*) "Runtime: ", finish-start
#endif


!close files and deallocate
close(3)
close(4)
close(5)
close(7)
close(8)

deallocate(plarticle_path,average,deviation)
deallocate(velocity,v_deviation,kernel2,KERNEL,g_forces)
#ifdef BOXED
deallocate(velo_appr_1,velo_appr_2,velo_appr_3,velo_appr_4)
deallocate(dkernel,ddkernel)
#endif







contains

subroutine initialize

!print *, "initializing..."
!initialize
do i = 0, iterations
  plarticle_path(i) = initial_value
  velocity(i) = initial_velocity
  average(i) = 0.D0
  deviation(i) = 0.D0
  v_deviation(i) = 0.D0
end do

do l = 1, histosize
  ! g_rand(1,l) = 0
  ! g_rand(2,l) = 0
  histo(l) = 0
  !fine_histo(l) = 0
  histo12(l) = 0
  histo15(l) = 0
  histo18(l) = 0
  histo21(l) = 0
  histo_clean(l) = 0
end do

do i=0,iteration2+1
  kernel2(i) = (1.D0*i)**(alpha) !FF2
end do

kernel_const2 = (time_interval**(alpha-1.D0))/( (alpha)*(alpha-1.D0) )

do k=0, iteration2 !FF2
  KERNEL(k) = kernel2(abs(k-1))-2.D0*kernel2(k)+ kernel2(abs(k+1))
  KERNEL(k) = kernel_const2*KERNEL(k)
end do

gforce_const = sqrt(2.D0*temperature*kernel_const2/time_interval)

return
end subroutine initialize

#ifdef BOXED
subroutine initialize_box
   
  open (unit = 2, file = "box_param.txt")
  do i = 1,4
    read (2,*) box_dens(i)
  end do
  read (2,*) box_window_size(1)
  read (2,*) odd_factor2
  read (2,*) odd_factor3
  read (2,*) odd_factor4
  box_window_size(2) = odd_factor2*box_window_size(1)
  box_window_size(3) = odd_factor3*box_window_size(2)
  box_window_size(4) = odd_factor4*box_window_size(3)
  do i=1,4
    write (4,*) "Box Start: ", box_dens(i), "Box Window Size: ", box_window_size(i)
  end do
  
  ndif = 10
  
  allocate( velo_appr_1(0:iterations/box_window_size(1)+1,3) )
  allocate( velo_appr_2(0:iterations/box_window_size(2)+1,3) )
  allocate( velo_appr_3(0:iterations/box_window_size(3)+1,3) )
  allocate( velo_appr_4(0:iterations/box_window_size(4)+1,3) )
  allocate( dkernel( 0:iteration2 ) )
  allocate( ddkernel( 0:iteration2 ) )
  
  do i = ndif,iteration2-ndif
    dkernel(i)=(KERNEL(i+ndif)-KERNEL(i-ndif))/(2.D0*ndif)
    ddkernel(i) = (KERNEL(i+ndif)-2*KERNEL(i)+KERNEL(i-ndif))/(1.D0*ndif)**2
    !print *, KERNEL(i), dkernel(i), ddkernel(i)
  end do
  
  close(2)
  
return
end subroutine initialize_box

subroutine approx_velos

  if (i.eq.1) then
    velo_appr_1(i1,:) = 0.D0
    velo_appr_2(i2,:) = 0.D0
    velo_appr_3(i3,:) = 0.D0
    velo_appr_4(i4,:) = 0.D0
  end if
  
  velo_appr_1(i1, 1) = velo_appr_1(i1, 1)+velocity(i-1)
  velo_appr_1(i1, 2) = velo_appr_1(i1, 2)+velocity(i-1)*d1
  velo_appr_1(i1, 3) = velo_appr_1(i1, 3)+velocity(i-1)*d1*d1
  
  d1 = d1+1
  
  if( d1==(box_window_size(1)/2) +1 ) then
    i1 = i1+1
    velo_appr_1(i1,:) = 0.D0
    d1 = -box_window_size(1)/2
  end if 
  
  velo_appr_2(i2, 1) = velo_appr_2(i2, 1)+velocity(i-1)
  velo_appr_2(i2, 2) = velo_appr_2(i2, 2)+velocity(i-1)*d2
  velo_appr_2(i2, 3) = velo_appr_2(i2, 3)+velocity(i-1)*d2*d2
  d2 = d2+1
  if( d2.eq.(box_window_size(2)/2+1) ) then
    i2 = i2+1
    velo_appr_2(i2,:) = 0.D0
    d2 = -box_window_size(2)/2
  end if 
  
  velo_appr_3(i3, 1) = velo_appr_3(i3, 1)+velocity(i-1)
  velo_appr_3(i3, 2) = velo_appr_3(i3, 2)+velocity(i-1)*d3
  velo_appr_3(i3, 3) = velo_appr_3(i3, 3)+velocity(i-1)*d3*d3
  d3 = d3+1
  if( d3.eq.(box_window_size(3)/2+1) ) then
    i3 = i3+1
    velo_appr_3(i3,:) = 0.D0
    d3 = -box_window_size(3)/2
  end if 

  
  
  velo_appr_4(i4, 1) = velo_appr_4(i4, 1)+velocity(i-1)
  velo_appr_4(i4, 2) = velo_appr_4(i4, 2)+velocity(i-1)*d4
  velo_appr_4(i4, 3) = velo_appr_4(i4, 3)+velocity(i-1)*d4*d4
  d4 = d4+1
  if( d4.eq.(box_window_size(4)/2+1) ) then
    i4 = i4+1
    velo_appr_4(i4,:) = 0.D0
    d4 = -box_window_size(4)/2
  end if 
  
  !if (i2.eq.1) print *, "v",i, i4, d4, velo_appr_4(i4, 1)
  !Sif (i2.eq.1) print *, "v",i, velo_appr_4(i4, 2), velo_appr_4(i4, 3)
  
return
end subroutine approx_velos

subroutine ff_approx
integer(i4b) :: z,z1,z2,z3,z4 !indexing through different velo approx

      friction_sum = 0.D0

      z = 0
      z1 = 0
      z2 = 0
      z3 = 0
      z4 = 0
      do s = 0,(i-1)
        
        !if ( ( i==620000).and.(z<615500) ) print *, z, friction_sum
        
        if (z.eq.0) then
          friction_sum = friction_sum + ( velocity(z)*KERNEL(i-1) )/2
          z=z+1
        else if (z.eq.(i-1)) then
          friction_sum = friction_sum + ( velocity(z)*KERNEL(i-1-z) )/2
          exit
        else if ( ((z+box_window_size(4)/2 -1).le.(i-1-box_dens(4))).and.(z4 < iterations/box_window_size(4)+1 )) then
          friction_sum = friction_sum + velo_appr_4(z4,1)*KERNEL(i-1-z-box_window_size(4)/2) &
          - velo_appr_4(z4,2)*dkernel(i-1-z-box_window_size(4)/2) !+ 0.5D0*velo_appr_4(z4,3)*ddkernel(i-1-z-box_window_size(4)/2)
          z4 = z4+1
          z = z+box_window_size(4)
          z3 = odd_factor4*z4
          z2 = odd_factor3*odd_factor4*z4
          z1 = odd_factor2*odd_factor3*odd_factor4*z4
        else if ( ((z+box_window_size(3)/2-1).le.(i-1-box_dens(3))).and.(z3 .lt. iterations/box_window_size(3)+1 )) then
          friction_sum = friction_sum + velo_appr_3(z3,1)*KERNEL(i-1-z-box_window_size(3)/2) &
          - velo_appr_3(z3,2)*dkernel(i-1-z-box_window_size(3)/2) + 0.5D0*velo_appr_3(z3,3)*ddkernel(i-1-z-box_window_size(3)/2)
          z3 = z3+1
          z = z+box_window_size(3)
          z2 = odd_factor3*z3
          z1 = odd_factor2*odd_factor3*z3
        else if ( ((z+box_window_size(2)/2-1).le.(i-1-box_dens(2))).and.(z2 .lt. iterations/box_window_size(2)+1) ) then
          friction_sum = friction_sum + velo_appr_2(z2,1)*KERNEL(i-1-z-box_window_size(2)/2) & 
          - velo_appr_2(z2,2)*dkernel(i-1-z-box_window_size(2)/2) + 0.5D0*velo_appr_2(z2,3)*ddkernel(i-1-z-box_window_size(2)/2)
          z2 = z2+1
          z = z+box_window_size(2)
          z1 = odd_factor2*z2
        else if ( ((z+box_window_size(1)/2-1).le.(i-1-box_dens(1))).and.(z1 .lt. iterations/box_window_size(1)+1 )) then
          friction_sum = friction_sum + velo_appr_1(z1,1)*KERNEL(i-1-z-box_window_size(1)/2) &
          - velo_appr_1(z1,2)*dkernel(i-1-z-box_window_size(1)/2) + 0.5D0*velo_appr_1(z1,3)*ddkernel(i-1-z-box_window_size(1)/2)
          z1 = z1+1
          z = z+box_window_size(1)
        else
          friction_sum = friction_sum + ( velocity(z)*KERNEL(i-1-z) )
          z = z+1
        end if
        
      end do 
      
      friction_sum = fiction_force*friction_sum

return
end subroutine ff_approx
#endif

subroutine ff

friction_sum = 0.D0
      
       do s = 0,(i-1)
        if (s.eq.0) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else if (s.eq.(i-1)) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )
        end if
      end do 
      
      friction_sum = fiction_force*friction_sum

return
end subroutine ff



end program corremos






!function to determine the power of M so that
!total time is greater than 1000
FUNCTION M( dt,t )
implicit none

integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers
real(r8b)              :: dt, test,t
integer(i4b)           :: M,n

do n = 1, 1000
  test = dt*2.D0**n
  if (test > t) EXIT
end do

if (n.eq.1000) then 
  print *, "Something is probably wrong (M) "
end if

print *, n

M=n

END FUNCTION M
    












!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis  
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123  
! 
! 
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
! 
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
! 
! 
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit 
!        v093     Aug 13, 2012    changed inter representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION rkiss05()
      implicit none
     
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31
 
      real(r8b)             :: rkiss05  
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w 
      
      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1 
      else 
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then 
         y=idum+1 
      else 
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1 
      else 
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1 
      else 
         w=idum
      endif

      rdum=rkiss05()
      
      return
      end subroutine kissinit
      
! Subroutine CORVEC(xr,Ns,M)
!
! generates a 1d array of Ns Gaussian random numbers xr
! correlated according to a (translationally invariant)
! user-supplied correlation function corfunc(is,Ns)
! 
! uses Fourier filtering method
!
! history
!      v0.9         Dec  7, 2013:        first version, uses Tao Pang FFT
!      v0.91        Oct 11, 2017:        uses much faster FFT by Ooura (in fftsg.f)        
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE corvec(xr,Ns,M,gamma)
      USE FFT
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b)           :: Ns              ! number of sites, must be power of 2 
      integer(i4b)           :: M               ! Ns=2^M

      real(r8b)              :: xr(0:Ns-1)      ! random number array  
      !real(r8b)              :: cr(0:Ns-1)      ! correlation function 
      real(r8b),allocatable  :: cr(:),w(:)           ! correlation function 
      integer(i4b)           :: is
      
      !integer(i4b)           :: ip(0:Int(2+sqrt(1.*Ns)))   ! workspace for FFT code
      integer(i4b), allocatable :: ip(:)   ! workspace for FFT code
      real(r8b)              :: gamma!,w(0:Ns/2-1)           ! workspace for FFT code 
      
      real(r8b), external    :: gkiss05,erfcc
      !real(r8b), external               :: rdft                   ! from Ooura's FFT package
      real(r8b),external     :: corfunc
      
      allocate(cr(0:Ns-1))
      allocate(w(0:Ns/2-1))
      allocate(ip(0:Int(2+sqrt(1.*Ns))))
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      if (Ns.ne.2**M) STOP 'Size indices do not match'
! Calculate correlation function 
      do is=0,Ns-1 
         cr(is)= corfunc(is,Ns,gamma) 
      enddo
! Real FFT of correlation function
       ip(0)=0
       call rdft(Ns, 1, cr, ip, w)  
       
! Create array of independent Gaussian random numbers
      do is=0,Ns-1
         xr(is)= gkiss05()
      enddo
! Real FFT of input random numbers      
       call rdft(Ns, 1, xr, ip, w)  
! Filter the Fourier components of the random numbers
! as real-space correlations are symmmetric, FT of c is real
      do is=1,Ns/2-1
          xr(2*is)=xr(2*is)*sqrt(abs(cr(2*is)))*2.D0/Ns
          xr(2*is+1)=xr(2*is+1)*sqrt(abs(cr(2*is)))*2.D0/Ns
      enddo
      xr(0)=xr(0)*sqrt(abs(cr(0)))*2.D0/Ns
      xr(1)=xr(1)*sqrt(abs(cr(1)))*2.D0/Ns 
      
! FFT of filtrered random numbers (back to real space)
       call rdft(Ns, -1, xr, ip, w)  
       
! Transform from Gaussian distribution to flat distribution on (0,1)      
      ! do is = 0,Ns-1
        ! xr(is) = 1 -  0.5D0*erfcc(xr(is)/sqrt(2.0D0)) 
      ! end do
      
      deallocate(cr,w,ip)
      
      return
      END SUBROUTINE corvec 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Desired correlation function of the random numbers in real space
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION corfunc(i,N,gamma)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)              :: corfunc
      real(r8b)              :: gamma, gam
      integer(i4b)           :: i,N
      integer(i4b)           :: dist

      gam = 2.D0 - gamma

      dist=min(i,N-i)
      if (dist.eq.0) then 
         corfunc=1.D0
      else   
         corfunc = 0.5D0*( (dist+1)**(2.D0-gam) - 2.D0*(dist**(2.D0-gam)) + (dist-1)**(2.D0-gam) ) 
      endif   
      
      return
      END FUNCTION corfunc


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Gaussian random number generator gkiss05
!
! generates normally distributed independent random numbers
! (zero mean, variance 1) using Box-Muller method in polar form
!
! uniform random numbers provided by Marsaglia's kiss (2005 version)
!
! before using the RNG, call gkissinit(seed) to initialize
! the generator. Seed should be a positive integer.
!
!
! History:
!      v0.9     Dec  6, 2013:   first version
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION gkiss05()
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)             :: gkiss05
      real(r8b), external   :: rkiss05

      real(r8b)             :: v1,v2,s,fac
      integer(i4b)          :: iset               ! switches between members of the Box-Muller pair
      real(r8b)             :: gset
      common /gausscom/gset,iset

      if (iset.ne.1) then
        do
          v1 = 2.D0 * rkiss05() - 1.D0
          v2 = 2.D0 * rkiss05() - 1.D0
          s = v1 * v1 + v2 * v2
          if ((s<1.D0) .and. (s>0.D0)) exit
        enddo
! Box-Muller transformation creates pairs of random numbers
        fac = sqrt(-2.D0 * log(s) / s)
        gset = v1 * fac
        iset = 1
        gkiss05 = v2 * fac
      else
        iset = 0
        gkiss05 = gset
      end if
      return
      END FUNCTION gkiss05


      SUBROUTINE gkissinit(iinit)
      implicit none
      integer,parameter     :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b)          :: iinit,iset
      real(r8b)             :: gset
      common /gausscom/gset,iset

      iset=0                         ! resets the switch between the members of the Box-Muller pair
      call kissinit(iinit)           ! initializes the rkiss05 RNG
      end subroutine gkissinit

 FUNCTION erfcc(x)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculates complementary error function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      real(r8b)   :: erfcc,x
      real(r8b)   :: t,z
      z=abs(x)
      t=1.D0/(1.D0+0.5D0*z)
      erfcc=t*exp(-z*z-1.26551223D0+t*(1.00002368D0+t*(.37409196D0+t*&
     &(.09678418D0+t*(-.18628806D0+t*(.27886807D0+t*(-1.13520398D0+t*&
     &(1.48851587D0+t*(-.82215223D0+t*.17087277D0)))))))))
      if (x.lt.0.D0) erfcc=2.D0-erfcc
      return
      END  
      
