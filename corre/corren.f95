!--------------------------------------------------
!Author: Sarah Skinner     Instructor: Thomas Vojta
!Program: Corren.f95     Project: Parry the Particle
!Last Edited: 6.4.2018
!--------------------------------------------------
!Program Description: This program generates the 
!    average, deviation, and histogram of the 
!    simulation of particles in a one-dimensional
!    space with a specified number of random forces 
!    interacting on each particle within a
!    specified time. The random numbers generated
!    are correlated with a gaussian probability
!    density.
!Program Notes: number of particles >= 10
!    1<alpha<2
!    
!--------------------------------------------------


program corren
USE FFT
implicit none

integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
integer,parameter          :: data_size = 5, histosize = 200
real(r8b),parameter        :: C_KE = 8.D0, C_10 = -5.D0
integer(i4b)               :: particle_num, outliers,temp_particle_num
integer(i4b)               :: i, j,k,l, x,m,h,s
real(r8b)                  :: random_force_y, wall_force
real(r8b)                  :: amplitude, temperature
                                             !for wall force  for friction force
real(r8b)                  :: fiction_force, alpha_const ,    alpha
real(r8b)                  :: initial_value, maxpos = 0.D0, minpos = 0.D0
real(r8b)                  :: initial_velocity, hardness, previously
integer(i4b)               :: iterations, negative_range, iteration_power
real(r8b)                  :: temp_particle, friction_sum, eta, eta_const
real(r8b)                  :: time_interval, short_time_interval
real(r8b)                  :: histo(histosize), temp
real(r8b)                  :: average_temp,deviation_temp,KE_temp,v_deviation_temp
real(r8b), allocatable     :: average(:),deviation(:),plarticle_path(:), velocity(:)
real(r8b), allocatable     :: KE(:),v_deviation(:), kernel1(:), kernel2(:)
real(r8b), allocatable     :: KERNEL(:), g_forces(:)
integer(i4b)               :: seed, stop1, stop2, stop3, stop4 
                           !stops for output files at certain intervals
real(r8b)                  :: time,absorb, g_rand(histosize)
logical                    :: time_choice
real(r8b)                  :: rkiss05, gkiss05, kernel_const, kernel_const2
character(50)              :: c !for naming file at certain intervals
character(50)              :: filename = 'output'
character(4)               :: ending = '.dat'











200 FORMAT(" ",f6.2," ", 33f20.8)

!open files
open(unit = 2, file = "sample.txt")
open(unit = 3, file = "output.dat")
open(unit = 4, file = "parameters.dat")
open(unit = 5, file = "histo.dat")
!unit = 6 is bad luck
open(unit = 7, file = "simplehisto.dat")
open(unit = 8, file = "gkiss.dat")

!initialize variables
fiction_force = 1.D0

!output to parameters.dat
write (4,*) "Friction force coefficient set to 1"

write (4,*) "Mass is set to one."

read (2,*) temperature
write (4,*) "Temperature:", temperature

read (2,*) initial_value
write (4,*) "initial value:", initial_value

read (2,*) particle_num
write (4,*) "number of particles: ", particle_num

stop1 = particle_num/10
stop2 = particle_num/4
stop3 = particle_num/2
stop4 = 3*(particle_num/4)

read (2,*) initial_velocity
write (4,*) "initial velocity: ", initial_velocity

read (2,*) time_interval
write (4,*) "time interval: ", time_interval

read (2,*) iteration_power !power for 2
iterations = 2**iteration_power
write (4,*) "total number of random forces to interact: ", iterations
time = time_interval*iterations*1.D0

read (2,*) alpha
write (4,*) "Alpha for friction force set to ", alpha

write (4,*) "Stops:",stop1, stop2,stop3,stop4
seed = 0

write(4,*) "Friction Force: FF2"

!temperature = sqrt(temperature)














!allocate arrays
allocate(plarticle_path(iterations))
allocate(average(iterations))
allocate(velocity(iterations))
allocate(deviation(iterations))
allocate(v_deviation(iterations))
allocate(KE(iterations))
allocate(kernel1(iterations+1))
allocate(kernel2(iterations+1))
allocate(KERNEL(iterations))
allocate(g_forces(iterations))


!set each particle value to initial value

!initialize average and deviation and histogram for specific iteration
do i = 0, iterations
  plarticle_path(i) = initial_value
  velocity(i) = initial_velocity
  average(i) = 0.D0
  deviation(i) = 0.D0
  v_deviation(i) = 0.D0
  KE(i) = 0.D0
end do

do l = 1, histosize
  g_rand(l) = 0
  histo(l) = 0
end do

do i=0,iterations+1
  kernel1(i) = (1.D0*i)**(alpha-1)
  kernel2(i) = (1.D0*i)**(alpha) !FF2
end do

do k=0, iterations !FF2
  KERNEL(k) = -2.D0*kernel2(k)
  if ( k.ne.0 ) then
    KERNEL(k) = KERNEL(k) + kernel2(k-1)
  end if
    KERNEL(k) = KERNEL(k) + kernel2(k+1)
end do

kernel_const = time_interval**(alpha-1)
kernel_const2 = time_interval**(alpha-1)/( (alpha)*(alpha-1) )
  
k=0
write (3,*) "Time:    Average:     Deviation:     KE:"














!run particles through walk by iterations
do j = 1,particle_num
    plarticle_path(0) = initial_value
    velocity(0) = initial_velocity
    seed = k;
    !call kissinit(seed)
    call gkissinit(seed)
    call corvec( g_forces, iterations, iteration_power)
    

    
    !go through each particle and put them through another interation
    do i = 1,iterations
    
      !seed based off of total iterations for all particles
      
      
      
      random_force_y = temperature*(g_forces(i)-0.5D0)
      
      !force interacts with particle and calculates new position and velocity
      plarticle_path(i) = plarticle_path(i-1) + velocity(i-1) * time_interval
      
      
      
      
      !physical wall
      !wall_force = C_KE*(10.D0**C_10)*exp(-1.D0*plarticle_path(i)/alpha_const)
      wall_force = 0.D0
      
      
      
      
      !not correct friction force
      !velocity = velocity + (-fiction_force*velocity+ random_force_y+wall_force)*time_interval
      
      
      
      
      
      ! friction force with gaussian profile - 1
      ! write(4,*) "Friction Force: FF1"
      ! friction_sum = 0.D0
      ! do s = 0,(i-1)
        ! friction_sum = friction_sum - kernel_const*(plarticle_path(s+1)-plarticle_path(s)) &
          ! *( kernel1(i-(s+1)) - kernel1(i-s) )/(alpha-1)
      ! end do
      ! friction_sum = -(fiction_force*friction_sum)
      ! velocity(i) = velocity(i-1) + friction_sum + eta*random_force_y*time_interval
      
      
      
      
      !friction force with gaussian profile - 2
      
      friction_sum = 0.D0
      do s = 0,(i-1)
        if (s.eq.0) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else if (s.eq.(i-1)) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )
        end if
      end do
      !print *, plarticle_path(i), velocity(i-1), random_force_y, friction_sum
      friction_sum = fiction_force*kernel_const2*friction_sum
      velocity(i) = velocity(i-1) - friction_sum + random_force_y*time_interval
      
      
      
      
      !print *, random_force_y, friction_sum, velocity(i), plarticle_path(i)

  
  
  
  
  
      !continuously calculate averages
      if( abs(plarticle_path(i))<1000.D0) then
        KE(i) = KE(i) + 0.5D0*(velocity(i)**2)
        average(i) = average(i) + plarticle_path(i)
        deviation(i) = deviation(i) + (plarticle_path(i)-initial_value)**2
        v_deviation(i) = v_deviation(i) + (velocity(i)-initial_velocity)**2
        if (plarticle_path(i)>maxpos) then
          maxpos = plarticle_path(i)
        end if
        if (plarticle_path(i)<minpos) then
          minpos = plarticle_path(i)
        end if
      else
        call exit()
      end if
      
      
      
      !print *, "particle:",j," ", plarticle_path(i),maxpos
      
      !for seed above
      k = k+1
      
      x = g_forces(i)*100
      if (x<200 .and. x>0) then
        g_rand(x) = g_rand(x) + 1
      end if
      
    end do
    
    
    
    
    !export data at particle numbers
    if ((j.eq.stop1).or.(j.eq.stop2).or.(j.eq.stop3).or.(j.eq.stop4)) then
        
      temp_particle_num = j
      filename = 'output'
      write( c, '(i0)' ) temp_particle_num
      filename = trim(filename)// trim(c) // ending
      open(unit = 9, file = trim(filename))
      
      do i = 0,iterations
        average_temp = average(i)/temp_particle_num
        deviation_temp = deviation(i)/temp_particle_num
        KE_temp = KE(i)/temp_particle_num
        v_deviation_temp = v_deviation(i)/temp_particle_num
        write (9,200) time_interval*i, average_temp, deviation_temp, KE_temp, v_deviation_temp
      end do
      
      
      close(9)
      
    end if
    
    
    
    
    
    
    !add to histograms
    x = (plarticle_path(iterations-1)*1000.D0)
    if ((x<50).and.(x>-50)) then
      histo(x+50) = histo(x+50) + 1
    end if
    
    
    print *, "particle:",j," ",x,minpos,maxpos
    
    
  
  
  
end do













!normalize histogram and finish calculating averages


!output
do i = 0,iterations
  average(i) = average(i)/particle_num
  deviation(i) = deviation(i)/particle_num
  KE(i) = KE(i)/particle_num
  v_deviation(i) = v_deviation(i)/particle_num
  write (3,200) time_interval*i, average(i), deviation(i), v_deviation(i), KE(i)
end do


!output final histo
do l = 1, histosize
  temp = l
  histo(l) = histo(l)/particle_num*1.D0
  g_rand(l) = g_rand(l)/particle_num*1.D0
  write (7, *) (temp-50)/1000.D0, histo(l)
  write (8, *) (temp)/200.D0, g_rand(l)
end do

!output to parameters
write (4,*) "Maximum position:", maxpos
write (4,*) "Minimum position:", minpos
write (4,*) "Outliers:", outliers

!close files and deallocate
close(2)
close(3)
close(4)
close(5)
close(7)
close(8)
deallocate(plarticle_path,average,deviation,KE)


end program corren




















!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis  
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123  
! 
! 
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
! 
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
! 
! 
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit 
!        v093     Aug 13, 2012    changed inter representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION rkiss05()
      implicit none
     
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31
 
      real(r8b)             :: rkiss05  
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w 
      
      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1 
      else 
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then 
         y=idum+1 
      else 
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1 
      else 
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1 
      else 
         w=idum
      endif

      rdum=rkiss05()
      
      return
      end subroutine kissinit
      
! Subroutine CORVEC(xr,Ns,M)
!
! generates a 1d array of Ns Gaussian random numbers xr
! correlated according to a (translationally invariant)
! user-supplied correlation function corfunc(is,Ns)
! 
! uses Fourier filtering method
!
! history
!      v0.9         Dec  7, 2013:        first version, uses Tao Pang FFT
!      v0.91        Oct 11, 2017:        uses much faster FFT by Ooura (in fftsg.f)        
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE corvec(xr,Ns,M)
      USE FFT
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b)           :: Ns              ! number of sites, must be power of 2 
      integer(i4b)           :: M               ! Ns=2^M

      real(r8b)              :: xr(0:Ns-1)      ! random number array  
      real(r8b)              :: cr(0:Ns-1)      ! correlation function 
      integer(i4b)           :: is
      
      integer(i4b)           :: ip(0:Int(2+sqrt(1.*Ns)))   ! workspace for FFT code
      real(r8b)              :: w(0:Ns/2-1)           ! workspace for FFT code 
      
      real(r8b), external    :: gkiss05,erfcc
      !real(r8b), external               :: rdft                   ! from Ooura's FFT package
      real(r8b),external     :: corfunc 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      if (Ns.ne.2**M) STOP 'Size indices do not match'
! Calculate correlation function 
      do is=0,Ns-1 
         cr(is)= corfunc(is,Ns) 
      enddo
! Real FFT of correlation function
       ip(0)=0
       call rdft(Ns, 1, cr, ip, w)  
       
! Create array of independent Gaussian random numbers
      do is=0,Ns-1
         xr(is)= gkiss05()
      enddo
! Real FFT of input random numbers      
       call rdft(Ns, 1, xr, ip, w)  
! Filter the Fourier components of the random numbers
! as real-space correlations are symmmetric, FT of c is real
      do is=1,Ns/2-1
          xr(2*is)=xr(2*is)*sqrt(abs(cr(2*is)))*2.D0/Ns
          xr(2*is+1)=xr(2*is+1)*sqrt(abs(cr(2*is)))*2.D0/Ns
      enddo
      xr(0)=xr(0)*sqrt(abs(cr(0)))*2.D0/Ns
      xr(1)=xr(1)*sqrt(abs(cr(1)))*2.D0/Ns 
      
! FFT of filtrered random numbers (back to real space)
       call rdft(Ns, -1, xr, ip, w)  
       
! Transform from Gaussian distribution to flat distribution on (0,1)      
      do is = 0,Ns-1
        xr(is) = 1 -  0.5D0*erfcc(xr(is)/sqrt(2.0D0)) 
      end do
      
      return
      END SUBROUTINE corvec 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Desired correlation function of the random numbers in real space
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION corfunc(i,N)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)              :: corfunc
      real(r8b)              :: gam
      integer(i4b)           :: i,N
      integer(i4b)           :: dist

      gam = 0.8D0

      dist=min(i,N-i)
      if (dist.eq.0) then 
         corfunc=1.D0
      else   
         corfunc = 0.5D0*( (dist+1)**(2.D0-gam) - 2.D0*(dist**(2.D0-gam)) + (dist-1)**(2.D0-gam) ) 
      endif   

      return
      END FUNCTION corfunc


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Gaussian random number generator gkiss05
!
! generates normally distributed independent random numbers
! (zero mean, variance 1) using Box-Muller method in polar form
!
! uniform random numbers provided by Marsaglia's kiss (2005 version)
!
! before using the RNG, call gkissinit(seed) to initialize
! the generator. Seed should be a positive integer.
!
!
! History:
!      v0.9     Dec  6, 2013:   first version
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION gkiss05()
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)             :: gkiss05
      real(r8b), external   :: rkiss05

      real(r8b)             :: v1,v2,s,fac
      integer(i4b)          :: iset               ! switches between members of the Box-Muller pair
      real(r8b)             :: gset
      common /gausscom/gset,iset

      if (iset.ne.1) then
        do
          v1 = 2.D0 * rkiss05() - 1.D0
          v2 = 2.D0 * rkiss05() - 1.D0
          s = v1 * v1 + v2 * v2
          if ((s<1.D0) .and. (s>0.D0)) exit
        enddo
! Box-Muller transformation creates pairs of random numbers
        fac = sqrt(-2.D0 * log(s) / s)
        gset = v1 * fac
        iset = 1
        gkiss05 = v2 * fac
      else
        iset = 0
        gkiss05 = gset
      end if
      return
      END FUNCTION gkiss05


      SUBROUTINE gkissinit(iinit)
      implicit none
      integer,parameter     :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b)          :: iinit,iset
      real(r8b)             :: gset
      common /gausscom/gset,iset

      iset=0                         ! resets the switch between the members of the Box-Muller pair
      call kissinit(iinit)           ! initializes the rkiss05 RNG
      end subroutine gkissinit

 FUNCTION erfcc(x)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculates complementary error function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      real(r8b)   :: erfcc,x
      real(r8b)   :: t,z
      z=abs(x)
      t=1.D0/(1.D0+0.5D0*z)
      erfcc=t*exp(-z*z-1.26551223D0+t*(1.00002368D0+t*(.37409196D0+t*&
     &(.09678418D0+t*(-.18628806D0+t*(.27886807D0+t*(-1.13520398D0+t*&
     &(1.48851587D0+t*(-.82215223D0+t*.17087277D0)))))))))
      if (x.lt.0.D0) erfcc=2.D0-erfcc
      return
      END  
      
