!--------------------------------------------------
!Author: Sarah Skinner     Instructor: Thomas Vojta
!Program: Corren.f95     Project: Parry the Particle
!Last Edited: 11.3.2018
!--------------------------------------------------
!Program Description: This program generates the 
!    average, deviation, and histogram of the 
!    simulation of particles in a one-dimensional
!    space with a specified number of random forces 
!    interacting on each particle within a
!    specified time. The random numbers generated
!    are correlated with a gaussian probability
!    density.
!Program Notes:  1<alpha<2
!If you set M = 0 in input file sample.txt
!then the program will automatically calc
!an M so that total time will be greater than
!1000
!    
!--------------------------------------------------


program corren
USE FFT
implicit none

integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
integer,parameter          :: data_size = 5, histosize = 200
real(r8b)                  :: C_KE, C_10
integer(i4b)               :: particle_num, outliers,temp_particle_num
integer(i4b)               :: i, j,k,l, x,m,h,s
real(r8b)                  :: random_force_y, wall_force
real(r8b)                  :: amplitude, temperature, F0
                                             !for wall force  for friction force
real(r8b)                  :: fiction_force, lambda ,    alpha
real(r8b)                  :: initial_value, maxpos = 0.D0, minpos = 0.D0
real(r8b)                  :: initial_velocity, hardness, previously
integer(i4b)               :: iterations, negative_range, iteration_power
real(r8b)                  :: temp_particle, friction_sum, eta, eta_const
real(r8b)                  :: time_interval, short_time_interval
real(r8b)                  :: histo(histosize), fine_histo(histosize), temp
real(r8b)                  :: average_temp,deviation_temp,v_deviation_temp
real(r8b), allocatable     :: average(:),deviation(:),plarticle_path(:), velocity(:)
real(r8b), allocatable     :: v_deviation(:), kernel1(:), kernel2(:)
real(r8b), allocatable     :: KERNEL(:), g_forces(:)
integer(i4b)               :: stop1, stop2, stop3, stop4, wall_choice
                           !stops for output files at certain intervals
real(r8b)                  :: time,absorb, g_rand(2,histosize), gforce_const
real(r8b)                  :: rkiss05, gkiss05, kernel_const, kernel_const2
character(50)              :: c !for naming file at certain intervals
character(50)              :: filename = 'output'
character(4)               :: ending = '.dat', nonsense

real::start, finish
call cpu_time(start)

200 FORMAT(" ",f6.2," ", 33f20.8)
201 FORMAT(" ",a6," ", 33a20)

!open files
open(unit = 2, file = "sample.txt")
open(unit = 3, file = "output.dat")
open(unit = 4, file = "parameters.dat")
open(unit = 5, file = "finehisto.dat")
!unit = 6 is bad luck
open(unit = 7, file = "simplehisto.dat")
open(unit = 8, file = "gkiss.dat")

!initialize variables, output to parameters.dat
fiction_force = 1.D0
write (4,*) "Friction force coefficient set to 1"
write (4,*) "Mass is set to one."
read (2,*) nonsense, temperature
write (4,*) "Temperature:", temperature
read (2,*) nonsense, initial_value
write (4,*) "initial value:", initial_value
read (2,*) nonsense, initial_velocity
write (4,*) "initial velocity: ", initial_velocity
read (2,*) nonsense, particle_num
write (4,*) "number of particles: ", particle_num

stop1 = particle_num/10
stop2 = particle_num/4
stop3 = particle_num/2
stop4 = 3*(particle_num/4)

read (2,*) nonsense, time_interval
write (4,*) "time interval: ", time_interval
read (2,*) nonsense, iteration_power !power for 2
if (iteration_power.eq.0) then
  iteration_power = M( time_interval )
end if
iterations = 2**iteration_power
write (4,*) "total number of random forces to interact: ", iterations
time = time_interval*iterations*1.D0
write (4,*) "total time: ", time
read (2,*) nonsense, alpha
write (4,*) "Alpha for friction force set to ", alpha
write (4,*) "Stops:",stop1, stop2,stop3,stop4
write(4,*) "Friction Force: FF2"
read (2,*) nonsense, lambda
write(4,*) "Alpha for wall force set to ", lambda
read (2,*) nonsense, F0
write(4,*) "Constant in front of wall force set to ", F0



!allocate arrays
allocate(plarticle_path(0:iterations))
allocate(average(0:iterations))
allocate(velocity(0:iterations))
allocate(deviation(0:iterations))
allocate(v_deviation(0:iterations))
allocate(kernel1(0:iterations+1))
allocate(kernel2(0:iterations+1))
allocate(KERNEL(0:iterations))
allocate(g_forces(0:iterations))

!initialize
do i = 0, iterations
  plarticle_path(i) = initial_value
  velocity(i) = initial_velocity
  average(i) = 0.D0
  deviation(i) = 0.D0
  v_deviation(i) = 0.D0
end do

do l = 1, histosize
  g_rand(1,l) = 0
  g_rand(2,l) = 0
  histo(l) = 0
  fine_histo(l) = 0
end do

do i=0,iterations+1
  kernel1(i) = (1.D0*i)**(alpha-1.D0)
  kernel2(i) = (1.D0*i)**(alpha) !FF2
end do

kernel_const2 = (time_interval**(alpha-1.D0))/( (alpha)*(alpha-1.D0) )

do k=0, iterations !FF2
  KERNEL(k) = kernel2(abs(k-1))-2.D0*kernel2(k)+ kernel2(abs(k+1))
  KERNEL(k) = kernel_const2*KERNEL(k)
end do

gforce_const = sqrt(2.D0*temperature*kernel_const2/time_interval)


















!walk each particle
do j = 1,particle_num
    plarticle_path(0) = initial_value
    velocity(0) = initial_velocity
    
    !make new forces for next run
    call gkissinit(j)
    call corvec( g_forces, iterations, iteration_power, alpha)


    !individual particle run
    do i = 1,iterations
    
      !calc new force for next run [-sqrt(temp),sqrt(temp)]
      random_force_y = gforce_const*(g_forces(i))
      
      !move particle
      plarticle_path(i) = plarticle_path(i-1) + velocity(i-1) * time_interval
      
      
      !physical wall
      wall_force = F0*exp(-1.D0*plarticle_path(i)*lambda)
      
      friction_sum = 0.D0
      do s = 0,(i-1)
        if (s.eq.0) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else if (s.eq.(i-1)) then
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )/2
        else
          friction_sum = friction_sum + ( velocity(s)*KERNEL(i-1-s) )
        end if
      end do
      !print *, plarticle_path(i), velocity(i-1), random_force_y, friction_sum
      friction_sum = fiction_force*friction_sum
      
      !calculate new velocity for next run
      velocity(i) = velocity(i-1) + ( -friction_sum + random_force_y + wall_force)*time_interval
      
      
      
      
      
      !add to averages and deviation and outlier handling
      if( abs(plarticle_path(i))<10000.D0) then
        average(i) = average(i) + plarticle_path(i)
        deviation(i) = deviation(i) + (plarticle_path(i)-initial_value)**2
        v_deviation(i) = v_deviation(i) + (velocity(i)-initial_velocity)**2
        if (plarticle_path(i)>maxpos) then
          maxpos = plarticle_path(i)
        end if
        if (plarticle_path(i)<minpos) then
          minpos = plarticle_path(i)
        end if
      else
        print *, "Something went wrong"
        call exit()
      end if
      
      
      
      
      
      
      !generating histograms for forces with mem
      x = (g_forces(i))*20
      if (x<100 .and. x>-100) then
        g_rand(1,x+100) = g_rand(1,x+100) + 1
      end if
      
      x = (gkiss05()/4.D0)*20
      if (x<100 .and. x>-100) then
        g_rand(2,x+100) = g_rand(2,x+100) + 1
      end if
      
    end do
    
    
    
    
    !generate temporary output files in case of crash
    if ((j.eq.stop1).or.(j.eq.stop2).or.(j.eq.stop3).or.(j.eq.stop4)) then
        
      temp_particle_num = j
      filename = 'output'
      write( c, '(i0)' ) temp_particle_num
      filename = trim(filename)// trim(c) // ending
      open(unit = 9, file = trim(filename))
      
      write (9,201) "dt", "<x>", "<x^2>", "<v^2>"
      do i = 0,iterations
        average_temp = average(i)/temp_particle_num
        deviation_temp = deviation(i)/temp_particle_num
        v_deviation_temp = v_deviation(i)/temp_particle_num
        write (9,200) time_interval*i, average_temp, deviation_temp, v_deviation_temp
      end do
      
      close(9)
      
    end if
    
    !add final position to hisogram
    x = (plarticle_path(iterations-1)*(100.D0/(10.D0*sqrt(temperature))))
    if ((x<100).and.(x>-100)) then
      histo(x+100) = histo(x+100) + 1
    end if
    
    x = (plarticle_path(iterations-1)*50.D0)
    if ((x<100).and.(x>-100)) then
      fine_histo(x+100) = fine_histo(x+100) + 1
    end if
    
    !for visual checks
    print *, "particle:",j," ",x,minpos,maxpos

end do


!output
write (3,201) "dt", "<x>", "<x^2>", "<v^2>"
do i = 0,iterations
  average(i) = average(i)/particle_num
  deviation(i) = deviation(i)/particle_num
  v_deviation(i) = v_deviation(i)/particle_num
  write (3,200) time_interval*i, average(i), deviation(i), v_deviation(i)
end do

write(7,*) "dx   prob dens "
write(5,*) "dx   prob dens "
write(8,*) "dx prob_dens_gkiss prob_dens_corvec"
do l = 1, histosize
  temp = l
  !Discrete Prob Distribution
  g_rand(1,l) = g_rand(1,l)/(particle_num*1.D0*iterations)
  g_rand(2,l) = g_rand(2,l)/(particle_num*1.D0*iterations)
  !Probability Density Distribution (n/(Ndx))
  histo(l) = histo(l)*(100.D0/(10.D0*sqrt(temperature)))/(1.D0*particle_num)
  fine_histo(l) = fine_histo(l)*(50.D0)/(1.D0*particle_num)
  write (8, *) (temp-100)/20.D0, g_rand(1,l), g_rand(2,l)
  write (5, *) (temp-100)/(50.D0), fine_histo(l) !dx = 1/50
  write (7, *) (temp-100)/(100.D0/(10.D0*sqrt(temperature))), histo(l) !dx = 10sqrt(T)/100
end do

write (4,*) "Maximum position:", maxpos
write (4,*) "Minimum position:", minpos









call cpu_time(finish)
print *, "Runtime: ", finish-start
write (4,*) "Runtime: ", finish-start

!close files and deallocate
close(2)
close(3)
close(4)
close(5)
close(7)
close(8)
deallocate(plarticle_path,average,deviation)
deallocate(velocity,v_deviation,kernel1,kernel2,KERNEL,g_forces)



end program corren






!function to determine the power of M so that
!total time is greater than 1000
FUNCTION M( dt )
implicit none

integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers
real(r8b)              :: dt, test
integer(i4b)           :: M,n

do n = 1, 1000
  test = dt*2.D0**n
  if (test > 1000.D0) EXIT
end do

if (n.eq.1000) then 
  print *, "Something is probably wrong (M) "
end if

M=n

END FUNCTION M
    














!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis  
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123  
! 
! 
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
! 
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
! 
! 
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit 
!        v093     Aug 13, 2012    changed inter representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION rkiss05()
      implicit none
     
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31
 
      real(r8b)             :: rkiss05  
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w 
      
      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1 
      else 
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then 
         y=idum+1 
      else 
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1 
      else 
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1 
      else 
         w=idum
      endif

      rdum=rkiss05()
      
      return
      end subroutine kissinit
      
! Subroutine CORVEC(xr,Ns,M)
!
! generates a 1d array of Ns Gaussian random numbers xr
! correlated according to a (translationally invariant)
! user-supplied correlation function corfunc(is,Ns)
! 
! uses Fourier filtering method
!
! history
!      v0.9         Dec  7, 2013:        first version, uses Tao Pang FFT
!      v0.91        Oct 11, 2017:        uses much faster FFT by Ooura (in fftsg.f)        
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE corvec(xr,Ns,M,gamma)
      USE FFT
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b)           :: Ns              ! number of sites, must be power of 2 
      integer(i4b)           :: M               ! Ns=2^M

      real(r8b)              :: xr(0:Ns-1)      ! random number array  
      real(r8b)              :: cr(0:Ns-1)      ! correlation function 
      integer(i4b)           :: is
      
      integer(i4b)           :: ip(0:Int(2+sqrt(1.*Ns)))   ! workspace for FFT code
      real(r8b)              :: w(0:Ns/2-1),gamma           ! workspace for FFT code 
      
      real(r8b), external    :: gkiss05,erfcc
      !real(r8b), external               :: rdft                   ! from Ooura's FFT package
      real(r8b),external     :: corfunc 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      if (Ns.ne.2**M) STOP 'Size indices do not match'
! Calculate correlation function 
      do is=0,Ns-1 
         cr(is)= corfunc(is,Ns,gamma) 
      enddo
! Real FFT of correlation function
       ip(0)=0
       call rdft(Ns, 1, cr, ip, w)  
       
! Create array of independent Gaussian random numbers
      do is=0,Ns-1
         xr(is)= gkiss05()
      enddo
! Real FFT of input random numbers      
       call rdft(Ns, 1, xr, ip, w)  
! Filter the Fourier components of the random numbers
! as real-space correlations are symmmetric, FT of c is real
      do is=1,Ns/2-1
          xr(2*is)=xr(2*is)*sqrt(abs(cr(2*is)))*2.D0/Ns
          xr(2*is+1)=xr(2*is+1)*sqrt(abs(cr(2*is)))*2.D0/Ns
      enddo
      xr(0)=xr(0)*sqrt(abs(cr(0)))*2.D0/Ns
      xr(1)=xr(1)*sqrt(abs(cr(1)))*2.D0/Ns 
      
! FFT of filtrered random numbers (back to real space)
       call rdft(Ns, -1, xr, ip, w)  
       
! Transform from Gaussian distribution to flat distribution on (0,1)      
      ! do is = 0,Ns-1
        ! xr(is) = 1 -  0.5D0*erfcc(xr(is)/sqrt(2.0D0)) 
      ! end do
      
      return
      END SUBROUTINE corvec 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Desired correlation function of the random numbers in real space
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION corfunc(i,N,gamma)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)              :: corfunc
      real(r8b)              :: gamma, gam
      integer(i4b)           :: i,N
      integer(i4b)           :: dist

      gam = 2.D0 - gamma

      dist=min(i,N-i)
      if (dist.eq.0) then 
         corfunc=1.D0
      else   
         corfunc = 0.5D0*( (dist+1)**(2.D0-gam) - 2.D0*(dist**(2.D0-gam)) + (dist-1)**(2.D0-gam) ) 
      endif   

      return
      END FUNCTION corfunc


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Gaussian random number generator gkiss05
!
! generates normally distributed independent random numbers
! (zero mean, variance 1) using Box-Muller method in polar form
!
! uniform random numbers provided by Marsaglia's kiss (2005 version)
!
! before using the RNG, call gkissinit(seed) to initialize
! the generator. Seed should be a positive integer.
!
!
! History:
!      v0.9     Dec  6, 2013:   first version
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION gkiss05()
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)             :: gkiss05
      real(r8b), external   :: rkiss05

      real(r8b)             :: v1,v2,s,fac
      integer(i4b)          :: iset               ! switches between members of the Box-Muller pair
      real(r8b)             :: gset
      common /gausscom/gset,iset

      if (iset.ne.1) then
        do
          v1 = 2.D0 * rkiss05() - 1.D0
          v2 = 2.D0 * rkiss05() - 1.D0
          s = v1 * v1 + v2 * v2
          if ((s<1.D0) .and. (s>0.D0)) exit
        enddo
! Box-Muller transformation creates pairs of random numbers
        fac = sqrt(-2.D0 * log(s) / s)
        gset = v1 * fac
        iset = 1
        gkiss05 = v2 * fac
      else
        iset = 0
        gkiss05 = gset
      end if
      return
      END FUNCTION gkiss05


      SUBROUTINE gkissinit(iinit)
      implicit none
      integer,parameter     :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b)          :: iinit,iset
      real(r8b)             :: gset
      common /gausscom/gset,iset

      iset=0                         ! resets the switch between the members of the Box-Muller pair
      call kissinit(iinit)           ! initializes the rkiss05 RNG
      end subroutine gkissinit

 FUNCTION erfcc(x)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculates complementary error function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      real(r8b)   :: erfcc,x
      real(r8b)   :: t,z
      z=abs(x)
      t=1.D0/(1.D0+0.5D0*z)
      erfcc=t*exp(-z*z-1.26551223D0+t*(1.00002368D0+t*(.37409196D0+t*&
     &(.09678418D0+t*(-.18628806D0+t*(.27886807D0+t*(-1.13520398D0+t*&
     &(1.48851587D0+t*(-.82215223D0+t*.17087277D0)))))))))
      if (x.lt.0.D0) erfcc=2.D0-erfcc
      return
      END  
      
