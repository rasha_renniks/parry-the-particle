program test
implicit none

integer,parameter          :: r8b= SELECTED_REAL_KIND(P=14,R=99)
integer,parameter          :: i4b= SELECTED_INT_KIND(8)
integer,parameter          :: data_size = 5, histosize = 200
real(r8b),parameter        :: C_KE = 8.D0, C_10 = -5.D0
integer(i4b)               :: particle_num, outliers,temp_particle_num
integer(i4b)               :: i, j,k,l, x,m,h,s
real(r8b)                  :: random_force_y, wall_force
real(r8b)                  :: amplitude, kernel_sum
                                             !for wall force  for friction force
real(r8b)                  :: fiction_force, alpha_const ,    alpha
real(r8b)                  :: initial_value, maxpos = 0.D0, minpos = 0.D0
real(r8b)                  :: initial_velocity, hardness, previously
integer(i4b)               :: iterations, negative_range
real(r8b)                  :: temp_particle, friction_sum, eta, eta_const
real(r8b)                  :: time_interval, short_time_interval
real(r8b)                  :: histo(histosize), temp
real(r8b)                  :: average_temp,deviation_temp,KE_temp,v_deviation_temp
real(r8b), allocatable     :: average(:),deviation(:),plarticle_path(:), velocity(:)
real(r8b), allocatable     :: KE(:),v_deviation(:), kernel1(:), kernel2(:)
real(r8b), allocatable     :: KERNEL(:)
integer(i4b)               :: seed, stop1, stop2, stop3, stop4 
                           !stops for output files at certain intervals
real(r8b)                  :: time,absorb, g_rand(histosize)
logical                    :: time_choice
real(r8b)                  :: rkiss05, gkiss05, kernel_const, kernel_const2
character(50)              :: c !for naming file at certain intervals
character(50)              :: filename = 'output'
character(4)               :: ending = '.dat'











!initialize variables
fiction_force = 1.D0
hardness = 1.D0
absorb = 0.1D0
alpha_const = 1.D0
outliers = 0
negative_range = 0
!alpha = 1.1D0 !alpha must be in between 1 and 2 !now inputted through sample
              !boltzman              room temp K
eta_const = 1.D0 !( 1.38D0*10.D0**(-23) )*( 293.D0 )



!output to parameters.dat
iterations = 10000
time_interval = 0.01D0
alpha = 1.5

!allocate arrays
allocate(plarticle_path(iterations))
allocate(average(iterations))
allocate(velocity(iterations))
allocate(deviation(iterations))
allocate(v_deviation(iterations))
allocate(KE(iterations))
allocate(kernel1(iterations+1))
allocate(kernel2(iterations+1))
allocate(KERNEL(iterations))

!call MPI

!set each particle value to initial value

!initialize average and deviation and histogram for specific iteration

do i=0,iterations+1
  kernel1(i) = (1.D0*i)**(alpha-1)
  kernel2(i) = (1.D0*i)**(alpha) !FF2
end do

kernel_sum = 0.D0
do k=0, iterations !FF2
  KERNEL(k) = -2.D0*kernel2(k)
  if ( k.ne.0 ) then
    KERNEL(k) = KERNEL(k) + kernel2(k-1)
  end if
  !if ( k.ne.(iterations) ) then
    KERNEL(k) = KERNEL(k) + kernel2(k+1)
  !end if
  kernel_sum = kernel_sum + KERNEL(k)
end do


kernel_const = time_interval**(alpha-1)
!FF2
kernel_const2 = (time_interval**(alpha-1))/( (alpha)*(alpha-1) )
  
k=0;
print *, KERNEL(0)
print *, KERNEL(iterations/2)
print *, KERNEL(iterations)
print *, kernel_const2
print *, kernel_const2*kernel_sum

end program test