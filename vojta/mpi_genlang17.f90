	      program mpi_genlang17
        USE FFT
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
!     Solves 1d generalized Langevin equation with power law correlations
!
!     uses FFT by Ooura (in fftsg.f)
!     uses KISS05 random number generator
!     serial + parallel via conditional compilation
!
!     -------------------------------------------------------------------
!     History:
!     
!     genlang1      :   16 Sep 2018  first version 
!     genlang2      :   16 Sep 2018  fixed error (2H instead of H in denominator of ranforce amplitude)
!     mpi_genlang2  :   16 Sep 2018  1st MPI version
!     mpi_genlang3  :   16 Sep 2018  output on log time scale, add wall force  
!     mpi_genlang4  :   16 Sep 2018  precalculate friction kernel
!     mpi_genlang5  :   16 Sep 2018  uses corvec with allocatble rather than automatic arrays (avoid segfault for large NT)
!     mpi_genlang6  :   17 Sep 2018  use discrete FBM kernel for both friction and random force
!     mpi_genlang7  :   06 Oct 2018  calculate probability denity at final time
!     mpi_genlang8  :   09 Oct 2018  include code for uncorrelated case 
!     mpi_genlang9  :   18 Oct 2018  approximate convolution in friction force for efficiency
!     mpi_genlang10 :   26 Oct 2018  include second derivative in approximation, change FBM correlations for accuracy (Taylor expansion)  
!     mpi_genlang11 :   13 Nov 2018  include third derivative in approximation
!     mpi_genlang12 :   14 Nov 2018  cleanup of new code, improve performance
!     mpi_genlang13 :   16 Nov 2018  three different windows for approx of convolution 
!     mpi_genlang14 :   16 Nov 2018  four windows, store all window averages in one array to save memory 
!     mpi_genlang15 :   17 Nov 2018  output after NSKIP walkers per node
!     mpi_genlang16 :   18 Nov 2018  reduce communication by only sending data for predetermined output time
!     mpi_genlang17 :   20 Dec 2018  added distribution collection and output at nt of 2**(12,15,18,21)
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Preprocessor directives        
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!#define PARALLEL
#define VERSION 'mpi_genlang17' 


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Data types
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      implicit none
      integer,parameter         :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter         :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 
      integer,parameter         :: ilog=kind(.true.)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Simulation parameters
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      integer(i4b), parameter    :: NW=    1                 ! number of random walkers
      integer(i4b), parameter    :: M=     18, NT= 2**M           ! number of time steps, must be power of 2
      real(r8b),    parameter    :: DT=    0.01D0                 ! time step
      real(r8b),    parameter    :: X0=    1.D0, V0= 0.D0       ! initial position and velocity (with wall, start slightly to the right)

      logical(ilog),parameter    :: FRACTIONAL=.true.           ! true: fractional LE, false: normal LE              
      real(r8b),    parameter    :: TEMP=  1.D0                  ! temperature
      real(r8b),    parameter    :: ALPHA= 1.00D0                ! damping prefactor alpha 
      real(r8b),    parameter    :: GAMMA= 0.4D0                 ! exponent of power-law correlations (Set gam in fbmcorvec to same value!!!)         
      
      logical(ilog), parameter   :: APPROXCONV=.true.           ! use approximate rather than exact convolution sum
      integer(i4b), parameter    :: NEXAC=   1000                   ! number of time steps treated exactly
      integer(i4b), parameter    :: NAPP1=   5000, NWIN1=115      ! number of steps approximated using first window, NWIN1 must be odd.
      integer(i4b), parameter    :: NAPP2=  40000, NWIN2=11*NWIN1  ! number of steps approximated using second window, NWIN2 must be odd multiple of NWIN1
      integer(i4b), parameter    :: NAPP3= 300000, NWIN3=15*NWIN2 ! number of steps approximated using third window, NWIN3 must be odd multiple of NWIN2
      integer(i4b), parameter    :: NWIN4= 21*NWIN3               ! fourth window, NWIN4 must be odd multiple of NWIN3
      integer(i4b), parameter    :: NDIF=10                     ! step for derivative, must be smaller than NWIN1/2 
      
      real(r8b),    parameter    :: F0=5.D0                      ! amplitude of wall force f=f0*exp(-lam*(x+XXW))
      real(r8b),    parameter    :: LAM =5.D0                    ! decay constant of wall force   
      real(r8b),    parameter    :: XXW =0.25D0                  ! wall shift

      integer(i4b), parameter     :: NSKIP = 25                    ! output after NSKIP walkers per node
      integer(i4b), parameter     :: NOUT = 400                    ! max number of output times, used as dimension of sumarrays 
      logical,parameter           :: WRITEDISTRIB = .true.        ! write final xx distribution    
      integer(i4b), parameter     :: NBIN =  200                   ! number of bins for distribution
      real(r8b), parameter        :: MAXXX = 30.D0                   ! largest - xx for histogram   
            
      integer(i4b), parameter    :: IRINIT=1                     ! Random number seed, must be positive

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Internal constants - do not touch !
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      real(r8b),  parameter      :: WW= SQRT(2.D0*TEMP*ALPHA*DT**(-GAMMA)/(2.D0-GAMMA)/(1.D0-GAMMA))        ! scaled force amplitude for fractional case
      real(r8b),  parameter      :: WWN = SQRT(2.D0*TEMP*ALPHA/DT)                                          ! scaled force amplitude for mormal case
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Variable declarations
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      real(r8b)       :: xx(0:NT),vv(0:NT)                      ! current position, velocity
      real(r8b)       :: vv1av(0:NT)                            ! NWIN1 avearges: velocity, asymmetry, curvature storend in consecutive array elements it, it+, it+2
      real(r8b)       :: vv2av(0:NT)                            ! NWIN2 avearges: velocity, asymmetry, curvature storend in consecutive array elements it, it+, it+2
      real(r8b)       :: vv3av(0:NT)                            ! NWIN3 avearges: velocity, asymmetry, curvature storend in consecutive array elements it, it+, it+2
      real(r8b)       :: vv4av(0:NT)                            ! NWIN4 avearges: velocity, asymmetry, curvature storend in consecutive array elements it, it+, it+2
      real(r8b)       :: vv1aux,vv1asyaux,vv1curaux
      real(r8b)       :: vv2aux,vv2asyaux,vv2curaux
      real(r8b)       :: vv3aux,vv3asyaux,vv3curaux
      real(r8b)       :: vv4aux,vv4asyaux,vv4curaux

      real(r8b)       :: ranforce(0:2*NT-1)                     ! random force
      real(r8b)       :: friforce, extforce                     ! friction, external forces
      real(r8b)       :: kernel(0:NT-1)                         ! kernel in friction integral
      real(r8b)       :: dkernel(0:NT-1), ddkernel(0:NT-1)      ! first and second derivative of kernel
      real(r8b)       :: kernelaux

      integer(i4b)    :: outtime(0:NOUT)                         ! it indeces of output times      
      real(r8b)       :: sumxxnode(0:NOUT),sumxx2node(0:NOUT)           ! averages of position and position**2 in one mpi node
      real(r8b)       :: sumvvnode(0:NOUT),sumvv2node(0:NOUT)           ! averages of velocity and velocity**2 in one mpi node
      real(r8b)       :: sumxxav(0:NOUT),sumxx2av(0:NOUT)           ! averages of position and position**2 over all mpi nodes
      real(r8b)       :: sumvvav(0:NOUT),sumvv2av(0:NOUT)           ! averages of velocity and velocity**2 over all mpi nodes
    
      integer(i4b)    :: iw,totw,iskip                                ! counters for walkers
      integer(i4b)    :: it,itp,itout,itoutmax                                  ! counters for time steps t, t_prime, and output time
      integer(i4b)    :: it1aux,it2aux,it3aux,it4aux
      integer(i4b)    :: init, i

      real(r8b)       :: xmin                                    ! keeps track of smallest x reached

      real(r8b)       :: xxdis(-NBIN:NBIN)                    ! density histogram final
      real(r8b)       :: sumdis(-NBIN:NBIN)
      real(r8b)       :: auxdis(-NBIN:NBIN)
      
      real(r8b)       :: xxdis_clean(-NBIN:NBIN)                      ! density histogram final with averaging for clean up
      real(r8b)       :: sumdis_clean(-NBIN:NBIN)
      real(r8b)       :: auxdis_clean(-NBIN:NBIN)

      real(r8b)       :: xxdis12(-NBIN:NBIN)                    ! density histogram stop 2**12
      real(r8b)       :: sumdis12(-NBIN:NBIN)
      real(r8b)       :: auxdis12(-NBIN:NBIN)

      real(r8b)       :: xxdis15(-NBIN:NBIN)                    ! density histogram stop 2**15
      real(r8b)       :: sumdis15(-NBIN:NBIN)
      real(r8b)       :: auxdis15(-NBIN:NBIN)

      real(r8b)       :: xxdis18(-NBIN:NBIN)                    ! density histogram stop 2**18
      real(r8b)       :: sumdis18(-NBIN:NBIN)
      real(r8b)       :: auxdis18(-NBIN:NBIN)

      real(r8b)       :: xxdis21(-NBIN:NBIN)                    ! density histogram stop 2**21
      real(r8b)       :: sumdis21(-NBIN:NBIN)
      real(r8b)       :: auxdis21(-NBIN:NBIN)

      real(r8b)       :: xbin,PP, scalefac                      ! P(x) 
      integer(i4b)    :: ibin
      integer(i4b), parameter :: stop12 = 2**12, stop15 = 2**15, stop18 = 2**18, stop21 = 2**21
      integer(i4b), parameter :: cleanup_term = 100            !how many to average over for cleaning up the final distribution output
      real(r8b)       :: x_clean
      
      real(r8b), external :: gkiss05,fbmcorfunc
      external        :: gkissinit,corvec

      character       :: outfile*14 = 'genlang17w.dat'
      character       :: disfile*14 = 'disxxxxxxw.dat'
      character       :: cleandisfile*20 = 'disxxxxxxw_clean.dat'
      
#ifdef PARALLEL 
      include 'mpif.h'
      integer(i4b)              :: ierr, output_stop            !determines when the processors send data to collector
      integer(i4b)              :: id,myid                      ! process index
      integer(i4b)              :: numprocs                     ! total number of processes    
      integer(i4b)              :: status(MPI_STATUS_SIZE)

      real(r8b)                 :: aux(0:NOUT)
#else       
      real(r8b)       :: start, finish
      call cpu_time(start)
#endif

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Start of main program
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Set up MPI !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#ifdef PARALLEL      
      call MPI_INIT(ierr)
      call MPI_COMM_RANK( MPI_COMM_WORLD, myid, ierr )
      call MPI_COMM_SIZE( MPI_COMM_WORLD, numprocs, ierr )

      if (myid==0) then 
        print *,'Program ',VERSION,' runing on', numprocs, ' processes' 
        print *, 'NT=', NT, 'number of walkers = ', NW
      endif ! of if (myid==0)
      output_stop = 1000
#else 
      print *,'Program ',VERSION,' runing on single processor'
      print *, 'NT=', NT, 'number of walkers = ', NW
#endif       

! Precalculate output times
      it=0
      itout=0
      do while (it.lt.NT)
         outtime(itout)=it
         it = max(it+NINT(0.5D0/DT),int(it*1.05D0))
         itout=itout+1
      enddo      
      itoutmax=itout
      outtime(itoutmax)=NT
      
! Precalculate friction kernel and its derivatives
      do it=0,NT-1
         kernel(it)= 2.D0*fbmcorfunc(it,2*NT,GAMMA)*DT**(1.D0-GAMMA)*ALPHA/(2.D0-GAMMA)/(1.D0-GAMMA)
      enddo
      do it=2*NDIF,NT-2*NDIF-1
        dkernel(it)=0.5D0*(kernel(it+NDIF)-kernel(it-NDIF)) / NDIF 
        ddkernel(it)= 0.5D0 * ( kernel(it+NDIF)-2.D0*kernel(it)+kernel(it-NDIF) ) / (NDIF)**2                         ! includes prefactor 1/2 from Taylor expansion
      enddo

      
!  Zero averages over walkers
      sumxxav(:)=0.D0
      sumxx2av(:)=0.D0
      sumvvav(:)=0.D0
      sumvv2av(:)=0.D0
#ifdef PARALLEL      
      sumxxnode(:)=0.D0
      sumxx2node(:)=0.D0
      sumvvnode(:)=0.D0
      sumvv2node(:)=0.D0
#endif

      xxdis(:)=0.D0 
      xxdis_clean(:)=0.D0
      xxdis12(:)=0.D0 
      xxdis15(:)=0.D0 
      xxdis18(:)=0.D0 
      xxdis21(:)=0.D0 

! keep track of minimum x                  
      xmin=X0
      
      !open( unit = 5, file = "chekup.dat" )
! Loop over random walkers 
      iskip=0      
#ifdef PARALLEL
      walker_loop: do iw=myid+1,(NW/numprocs)*numprocs,numprocs
        if (myid==0) print *, 'walker', iw
#else
      walker_loop: do iw=1,NW
        if ( 10*(iw/10)==iw ) print *, 'walker ', iw 
#endif        

        iskip=iskip+1
        init=IRINIT+iw-1 
        call gkissinit(init)                        ! initialize RNG
        
        if (FRACTIONAL) then
          call corvec(ranforce,2*NT,M+1,GAMMA)               ! creates correlated Gaussian random numbers          
          ranforce(:)=WW*ranforce(:)                   ! scale random force       
        else
          do it=0,2*NT-1  
             ranforce(it)=WWN*gkiss05()                ! create uncorrelated random forces
          enddo 
        endif  
        xx(0)=X0                                    ! set initial position    
        vv(0)=V0                                    ! set initial velocity       
         
                  
! Loop over time steps fractional case
       if (FRACTIONAL) then
         vv1aux=0.D0
         vv2aux=0.D0
         vv3aux=0.D0
         vv4aux=0.D0
         vv1asyaux=0.D0
         vv2asyaux=0.D0
         vv3asyaux=0.D0
         vv4asyaux=0.D0
         vv1curaux=0.D0
         vv2curaux=0.D0
         vv3curaux=0.D0
         vv4curaux=0.D0
         it1aux=-NWIN1/2
         it2aux=-NWIN2/2
         it3aux=-NWIN3/2
         it4aux=-NWIN4/2
         
         x_clean = 0.D0
         time_loop: do it=0,NT-1
           xx(it+1)= xx(it) +DT*vv(it)                ! propagate position 
           if (xx(it+1).lt.xmin) xmin=xx(it+1)
           extforce = F0*exp(-LAM*(xx(it)+XXW))             ! repulsive force modelling the wall
           !if ( MOD(it, 1000)==0) write (5,*) it, xx(it)
           if (APPROXCONV) then
              call friforce_approx
           else
              call friforce_exact           
           endif
           vv(it+1)=vv(it) + DT*(ranforce(it)-friforce +extforce )   ! propagate velocity                     

           if( (it==(stop12-1)).and.WRITEDISTRIB ) then
              ibin=nint( xx(it+1)*NBIN/MAXXX )
    !          print *, xx(NT),ibin
              if ( (ibin.ge.-NBIN) .and. (ibin.le.NBIN)) then
                 xxdis12(ibin)=xxdis12(ibin)+1.D0 
              endif    
           endif
           
           if( (it==(stop15-1)).and.WRITEDISTRIB ) then
              ibin=nint( xx(it+1)*NBIN/MAXXX )
    !          print *, xx(NT),ibin
              if ( (ibin.ge.-NBIN) .and. (ibin.le.NBIN)) then
                 xxdis15(ibin)=xxdis15(ibin)+1.D0 
              endif    
           endif
           
           if( (it==(stop18-1)).and.WRITEDISTRIB ) then
              ibin=nint( xx(it+1)*NBIN/MAXXX )
    !          print *, xx(NT),ibin
              if ( (ibin.ge.-NBIN) .and. (ibin.le.NBIN)) then
                 xxdis18(ibin)=xxdis18(ibin)+1.D0 
              endif    
           endif
           
           if( (it==(stop21-1)).and.WRITEDISTRIB ) then
              ibin=nint( xx(it+1)*NBIN/MAXXX )
    !          print *, xx(NT),ibin
              if ( (ibin.ge.-NBIN) .and. (ibin.le.NBIN)) then
                 xxdis21(ibin)=xxdis21(ibin)+1.D0 
              endif    
           endif
           
           if (WRITEDISTRIB.and.(it>NT-cleanup_term-1)) then
              x_clean = x_clean + nint( xx(it+1)*NBIN/MAXXX )
           endif
           
           if (APPROXCONV) then
             vv1aux=vv1aux+vv(it)
             vv1asyaux=vv1asyaux+vv(it)*it1aux
             vv1curaux=vv1curaux+vv(it)*it1aux*it1aux
             it1aux=it1aux+1
             vv2aux=vv2aux+vv(it)
             vv2asyaux=vv2asyaux+vv(it)*it2aux
             vv2curaux=vv2curaux+vv(it)*it2aux*it2aux
             it2aux=it2aux+1
             vv3aux=vv3aux+vv(it)
             vv3asyaux=vv3asyaux+vv(it)*it3aux
             vv3curaux=vv3curaux+vv(it)*it3aux*it3aux
             it3aux=it3aux+1
             vv4aux=vv4aux+vv(it)
             vv4asyaux=vv4asyaux+vv(it)*it4aux
             vv4curaux=vv4curaux+vv(it)*it4aux*it4aux
             it4aux=it4aux+1
             if ( modulo(it+1,NWIN1).eq.0) then                    
                vv1av(it-NWIN1/2) = vv1aux
                vv1av(it-NWIN1/2+1) = vv1asyaux
                vv1av(it-NWIN1/2+2) = vv1curaux
                vv1aux=0.D0
                vv1asyaux=0.D0
                vv1curaux=0.D0
                it1aux=-NWIN1/2                
             endif           
             if ( modulo(it+1,NWIN2).eq.0) then  
                vv2av(it-NWIN2/2) = vv2aux
                vv2av(it-NWIN2/2+1) = vv2asyaux
                vv2av(it-NWIN2/2+2) = vv2curaux
                vv2aux=0.D0
                vv2asyaux=0.D0
                vv2curaux=0.D0
                it2aux=-NWIN2/2                
             endif           
             if ( modulo(it+1,NWIN3).eq.0) then                    
                vv3av(it-NWIN3/2) = vv3aux
                vv3av(it-NWIN3/2+1) = vv3asyaux
                vv3av(it-NWIN3/2+2) = vv3curaux
                vv3aux=0.D0
                vv3asyaux=0.D0
                vv3curaux=0.D0
                it3aux=-NWIN3/2                
             endif           
             if ( modulo(it+1,NWIN4).eq.0) then                    
                vv4av(it-NWIN4/2) = vv4aux
                vv4av(it-NWIN4/2+1) = vv4asyaux
                vv4av(it-NWIN4/2+2) = vv4curaux
                vv4aux=0.D0
                vv4asyaux=0.D0
                vv4curaux=0.D0
                it4aux=-NWIN4/2                
             endif           
           endif  
         enddo time_loop   
         
       else
       
         time_loop2: do it=0,NT-1
           xx(it+1)= xx(it) +DT*vv(it)                ! propagate position 
           if (xx(it+1).lt.xmin) xmin=xx(it+1)
           extforce = F0*exp(-LAM*(xx(it)+XXW))             ! repulsive force modelling the wall
           friforce = ALPHA*vv(it)
           vv(it+1)=vv(it) + DT*(ranforce(it)-friforce +extforce )   ! propagate velocity                            
         enddo time_loop2     
       endif  
       
!       do it=0,NT
!          print *, vv(it), vvav(it)
!       enddo
!       pause
       
       if (WRITEDISTRIB) then
          ibin=nint( xx(NT)*NBIN/MAXXX )
!          print *, xx(NT),ibin
          if ( (ibin.ge.-NBIN) .and. (ibin.le.NBIN)) then
             xxdis(ibin)=xxdis(ibin)+1.D0 
          endif    
          x_clean = x_clean/(1.D0*cleanup_term)
          if ( (x_clean.ge.-NBIN) .and. (x_clean.le.NBIN)) then
             xxdis_clean(x_clean)=xxdis_clean(x_clean)+1.D0
          endif
       endif
       

#ifdef PARALLEL      
       do itout=0,itoutmax
          sumxxnode(itout)= sumxxnode(itout)+xx(outtime(itout))
          sumxx2node(itout)=sumxx2node(itout)+xx(outtime(itout))**2
          sumvvnode(itout)= sumvvnode(itout)+vv(outtime(itout))
          sumvv2node(itout)=sumvv2node(itout)+vv(outtime(itout))**2
       enddo
#else
       do itout=0,itoutmax
          sumxxav(itout)= sumxxav(itout)+xx(outtime(itout))
          sumxx2av(itout)=sumxx2av(itout)+xx(outtime(itout))**2
          sumvvav(itout)= sumvvav(itout)+vv(outtime(itout))
          sumvv2av(itout)=sumvv2av(itout)+vv(outtime(itout))**2
       enddo
#endif

! MPI transmisson and output everegy NSKIP walkers
       if( (iskip/NSKIP)*NSKIP.eq.iskip) then     
        
        
#ifdef PARALLEL 
         if(myid.ne.0) then                                                  ! Send data
           call MPI_SEND(sumxxnode,NOUT+1,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
           call MPI_SEND(sumxx2node,NOUT+1,MPI_DOUBLE_PRECISION,0,2,MPI_COMM_WORLD,ierr)
           call MPI_SEND(sumvvnode,NOUT+1,MPI_DOUBLE_PRECISION,0,3,MPI_COMM_WORLD,ierr)
           call MPI_SEND(sumvv2node,NOUT+1,MPI_DOUBLE_PRECISION,0,4,MPI_COMM_WORLD,ierr)
         else                                                                 ! Receive data
           do id=0,numprocs-1
             if (id==0) then
               sumxxav(:)= sumxxnode(:)
               sumxx2av(:)=sumxx2node(:)
               sumvvav(:)= sumvvnode(:)
               sumvv2av(:)=sumvv2node(:)
             else
               call MPI_RECV(aux,NOUT+1,MPI_DOUBLE_PRECISION,id,1,MPI_COMM_WORLD,status,ierr)
               sumxxav(:) =sumxxav(:)+ aux(:)
               call MPI_RECV(aux,NOUT+1,MPI_DOUBLE_PRECISION,id,2,MPI_COMM_WORLD,status,ierr)
               sumxx2av(:)=sumxx2av(:)+aux(:)
               call MPI_RECV(aux,NOUT+1,MPI_DOUBLE_PRECISION,id,3,MPI_COMM_WORLD,status,ierr)
               sumvvav(:) =sumvvav(:) +aux(:)
               call MPI_RECV(aux,NOUT+1,MPI_DOUBLE_PRECISION,id,4,MPI_COMM_WORLD,status,ierr)
               sumvv2av(:)=sumvv2av(:)+aux(:)
             endif    
          enddo
       endif       
#endif       
 

      if (WRITEDISTRIB) then
#ifdef PARALLEL      
        if(myid.ne.0) then                                                  ! Send data
          call MPI_SEND(xxdis,2*NBIN+1,MPI_DOUBLE_PRECISION,0,5,MPI_COMM_WORLD,ierr)
          call MPI_SEND(xxdis_clean,2*NBIN+1,MPI_DOUBLE_PRECISION,0,10,MPI_COMM_WORLD,ierr)
          call MPI_SEND(xxdis12,2*NBIN+1,MPI_DOUBLE_PRECISION,0,6,MPI_COMM_WORLD,ierr)
          call MPI_SEND(xxdis15,2*NBIN+1,MPI_DOUBLE_PRECISION,0,7,MPI_COMM_WORLD,ierr)
          call MPI_SEND(xxdis18,2*NBIN+1,MPI_DOUBLE_PRECISION,0,8,MPI_COMM_WORLD,ierr)
          call MPI_SEND(xxdis21,2*NBIN+1,MPI_DOUBLE_PRECISION,0,9,MPI_COMM_WORLD,ierr)
        else
          sumdis(:)=xxdis(:)
          sumdis_clean(:)=xxdis_clean(:)
          sumdis12(:)=xxdis12(:)
          sumdis15(:)=xxdis15(:)
          sumdis18(:)=xxdis18(:)
          sumdis21(:)=xxdis21(:)
          do id=1,numprocs-1
            call MPI_RECV(auxdis,2*NBIN+1,MPI_DOUBLE_PRECISION,id,5,MPI_COMM_WORLD,status,ierr)
            call MPI_RECV(auxdis_clean,2*NBIN+1,MPI_DOUBLE_PRECISION,id,10,MPI_COMM_WORLD,status,ierr)
            call MPI_RECV(auxdis12,2*NBIN+1,MPI_DOUBLE_PRECISION,id,6,MPI_COMM_WORLD,status,ierr)
            call MPI_RECV(auxdis15,2*NBIN+1,MPI_DOUBLE_PRECISION,id,7,MPI_COMM_WORLD,status,ierr)
            call MPI_RECV(auxdis18,2*NBIN+1,MPI_DOUBLE_PRECISION,id,8,MPI_COMM_WORLD,status,ierr)
            call MPI_RECV(auxdis21,2*NBIN+1,MPI_DOUBLE_PRECISION,id,9,MPI_COMM_WORLD,status,ierr)
            sumdis(:)=sumdis(:)+auxdis(:)           
            sumdis_clean(:)=sumdis_clean(:)+auxdis_clean(:)           
            sumdis12(:)=sumdis12(:)+auxdis12(:)           
            sumdis15(:)=sumdis15(:)+auxdis15(:)           
            sumdis18(:)=sumdis18(:)+auxdis18(:)           
            sumdis21(:)=sumdis21(:)+auxdis21(:)           
          enddo 
        endif
#else
      sumdis(:)=xxdis(:)
      sumdis_clean(:)=xxdis_clean(:)
      sumdis12(:)=xxdis12(:)
      sumdis15(:)=xxdis15(:)
      sumdis18(:)=xxdis18(:)
      sumdis21(:)=xxdis21(:)
#endif
      endif
      
            
#ifdef PARALLEL
      totw=iw+numprocs-1
      if (myid==0) then
#else
      totw=iw
#endif  
         
                                
      open(7,file=outfile,status='replace')
      open(unit = 8, file = "outputb.dat", status='replace')
      rewind(7)
#ifdef PARALLEL
      write(7,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
      write(7,*) 'program ', VERSION ,' serial' 
#endif  
      write(7,*) 'Maximum time  ', NT*DT
      write(7,*) 'Time step ', DT
      write(7,*) 'Fractional ', FRACTIONAL
      write(7,*) 'Correlation exponent gamma', GAMMA  
      write(7,*) 'Damping constant alpha',  ALPHA
      write(7,*) 'TEMPERATURE   ',TEMP
      write(7,*) 'Approximate convolution ', APPROXCONV 
      write(7,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3  
      write(7,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
      write(7,*) 'Wall force constants F0, LAM',F0,LAM
      write(7,*) 'Wall force shift XXW',XXW
      write(7,*) 'RNG seed   ', IRINIT
      write(7,*) 'Number of walkers', totw, 'of ', NW
      write(7,*) '-----------------'
      write(7,*) ' time    xxav    xx2av     vvav     vv2av'
      do itout=0,itoutmax
         write(7,'(1x,25(e13.6,1x))') outtime(itout)*DT, sumxxav(itout)/totw,sumxx2av(itout)/totw,&
     &                                                   sumvvav(itout)/totw, sumvv2av(itout)/totw
         write(8,*) sumxx2av(itout)
      enddo
      close(7)
      close(8)

      if (WRITEDISTRIB) then
        write(disfile(4:9),'(I6.6)') NINT(NT*DT)
        open(2,file=disfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', NT*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis(ibin)*NBIN)/(MAXXX*totw)
          scalefac=sqrt(sumxx2av(itoutmax)/totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
     
        
        write(cleandisfile(4:9),'(I6.6)') NINT(NT*DT)
        open(2,file=cleandisfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', NT*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis_clean(ibin)*NBIN)/(MAXXX*totw)
          scalefac=sqrt(sumxx2av(itoutmax)/totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
        
        write(disfile(4:9),'(I6.6)') NINT(stop12*DT)
        open(2,file=disfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', stop12*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do itout=0,itoutmax
          if( outtime(itout).ge.stop12 ) exit
        enddo
        scalefac=sqrt( (sumxx2av(itout)+sumxx2av(itout-1))/(2.D0*totw) )
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis12(ibin)*NBIN)/(MAXXX*totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
        
        write(disfile(4:9),'(I6.6)') NINT(stop15*DT)
        open(2,file=disfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', stop15*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do itout=0,itoutmax
          if( outtime(itout).ge.stop15 ) exit
        enddo
        scalefac=sqrt( (sumxx2av(itout)+sumxx2av(itout-1))/(2.D0*totw) )
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis15(ibin)*NBIN)/(MAXXX*totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
        
        write(disfile(4:9),'(I6.6)') NINT(stop18*DT)
        open(2,file=disfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', stop18*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do itout=0,itoutmax
          if( outtime(itout).ge.stop18 ) exit
        enddo
        scalefac=sqrt( (sumxx2av(itout)+sumxx2av(itout-1))/(2.D0*totw) )
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis18(ibin)*NBIN)/(MAXXX*totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
        
        write(disfile(4:9),'(I6.6)') NINT(stop21*DT)
        open(2,file=disfile,status='replace')
        rewind(2)
#ifdef PARALLEL
        write(2,*) 'program ', VERSION ,' on ', numprocs, ' CPUs' 
#else
        write(2,*) 'program ', VERSION ,' serial' 
#endif  
        write(2,*) 'Density distribution at time', stop21*DT 
        write(2,*) 'Time step ', DT
        write(2,*) 'Fractional ', FRACTIONAL
        write(2,*) 'Correlation exponent gamma', GAMMA  
        write(2,*) 'Damping constant alpha',  ALPHA
        write(2,*) 'TEMPERATURE   ',TEMP
        write(2,*) 'NEXAC=', NEXAC, ' NAPP1=',NAPP1, ' NAPP2=',NAPP2, ' NAPP3=',NAPP3 
        write(2,*) 'NWIN1=',NWIN1, ' NWIN2=',NWIN2, ' NWIN3=',NWIN3, ' NWIN4=',NWIN4
        write(2,*) 'Wall force constants F0, LAM',F0,LAM
        write(2,*) 'Wall force shift XXW',XXW
        write(2,*) 'IRINIT=',IRINIT
        write(2,*) 'Number of walkers', totw, 'of ', NW
        write (2,*)'=================================='
        write(2,*) '   ibin    x=(MAXX*ibin)/NBIN   P(x)   x/sigma   P*sigma'
        do itout=0,itoutmax
          if( outtime(itout).ge.stop21 ) exit
        enddo
        scalefac=sqrt( (sumxx2av(itout)+sumxx2av(itout-1))/(2.D0*totw) )
        do ibin=-NBIN,NBIN 
          xbin= (MAXXX*ibin)/NBIN
          PP= (sumdis21(ibin)*NBIN)/(MAXXX*totw)
          !scalefac=sqrt(sumxx2av(stop21)/totw)
          Write(2,'(1X,I7,5(2X,E13.6))') ibin, xbin, PP, xbin/scalefac, PP*scalefac   
        enddo 
        close(2) 
        
      endif !writedistr
      
#ifdef PARALLEL
      endif ! of if (myid==0)
#endif

      endif    ! of  if( (iskip/NSKIP)*NSKIP.eq.iskip)

      enddo walker_loop
      
      !close(5)

      print *, 'minimum x reached ', xmin         
      
#ifdef PARALLEL
      call MPI_FINALIZE(ierr) 
#else
      call cpu_time(finish)
      print *, finish-start
#endif
                 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      contains
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      
      subroutine friforce_exact                   ! memory sum for friction force
      friforce=0.5D0*vv(it)*kernel(0)
      do itp=0,it-1
          friforce = friforce + vv(itp)*kernel(it-itp)
      enddo
      return
      end subroutine friforce_exact


      subroutine friforce_approx                  ! memory sum for friction force
      integer(i4b)    ::    Nx1,Nx2,Nx3,Nx4            ! cutoffs between exact and approx
      if (it.le.NEXAC) then 
         friforce=0.5D0*vv(it)*kernel(0)
         do itp=0,it-1  
           friforce = friforce + vv(itp)*kernel(it-itp)
         enddo
      else
         friforce=0.5D0*vv(it)*kernel(0)
         Nx4= max( ( (it-NEXAC-NAPP1-NAPP2-NAPP3)/NWIN4 )*NWIN4,0)
         Nx3= max( ( (it-Nx4-NEXAC-NAPP1-NAPP2)/NWIN3 )*NWIN3,0)
         Nx2= max( ( (it-Nx4-Nx3-NEXAC-NAPP1)/NWIN2 )*NWIN2,0)
         Nx1= max( ( (it-Nx4-Nx3-Nx2-NEXAC)/NWIN1 )*NWIN1,0)

         do itp=NWIN4/2,Nx4-1,NWIN4  
           friforce = friforce + vv4av(itp)*kernel(it-itp) - vv4av(itp+1)*dkernel(it-itp) + vv4av(itp+2)*ddkernel(it-itp)      ! - sign for dkernel because argument it-itp
         enddo
         do itp=Nx4+NWIN3/2,Nx4+Nx3-1,NWIN3  
           friforce = friforce + vv3av(itp)*kernel(it-itp) - vv3av(itp+1)*dkernel(it-itp) + vv3av(itp+2)*ddkernel(it-itp)      ! - sign for dkernel because argument it-itp
         enddo
         do itp=Nx4+Nx3+NWIN2/2,Nx4+Nx3+Nx2-1,NWIN2
           friforce = friforce + vv2av(itp)*kernel(it-itp) - vv2av(itp+1)*dkernel(it-itp) + vv2av(itp+2)*ddkernel(it-itp)     ! - sign for dkernel because argument it-itp
         enddo
         do itp=Nx4+Nx3+Nx2+NWIN1/2,Nx4+Nx3+Nx2+Nx1-1,NWIN1
           friforce = friforce + vv1av(itp)*kernel(it-itp) - vv1av(itp+1)*dkernel(it-itp) + vv1av(itp+2)*ddkernel(it-itp)     ! - sign for dkernel because argument it-itp
         enddo
         do itp=Nx4+Nx3+Nx2+Nx1,it-1  
           friforce = friforce + vv(itp)*kernel(it-itp)
         enddo        
      endif      
      return
      end subroutine friforce_approx      

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
      end program mpi_genlang17    ! Main Program ends !!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Subroutine CORVEC(xr,Ns,MM)
!
! generates a 1d array of Ns Gaussian random numbers xr
! correlated according to a (translationally invariant)
! user-supplied correlation function corfunc(is,Ns)
! 
! uses Fourier filtering method
!
! history
!      v0.9         Dec  7, 2013:        first version, uses Tao Pang FFT
!      v0.91        Oct 11, 2017:        uses much faster FFT by Ooura (in fftsg.f)        
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      SUBROUTINE corvec(xr,Ns,MM,gamm)
      USE FFT
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers 

      integer(i4b)           :: Ns              ! number of sites, must be power of 2 
      integer(i4b)           :: MM               ! Ns=2^M

      real(r8b)              :: xr(0:Ns-1), gamm      ! random number array  
      real(r8b),allocatable  :: cr(:)      ! correlation function 
      integer(i4b)           :: is
      integer(i4b)           :: ipsize      
      integer(i4b),allocatable :: ip(:)   ! workspace for FFT code
      real(r8b),allocatable  :: w(:)           ! workspace for FFT code 
      
      real(r8b), external    :: gkiss05,erfcc
      !external               :: rdft                   ! from Ooura's FFT package
      real(r8b),external     :: fbmcorfunc 
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      ipsize= 2+sqrt(1.*Ns)
      allocate(cr(0:Ns-1))
      allocate(ip(0:ipsize))
      allocate(w(0:Ns/2-1))
      if (Ns.ne.2**MM) STOP 'Size indices do not match'
! Calculate correlation function 
      do is=0,Ns-1 
         cr(is)= fbmcorfunc(is,Ns,gamm) 
      enddo
! Real FFT of correlation function
       ip(0)=0
       call rdft(Ns, 1, cr, ip, w)  
       
! Create array of independent Gaussian random numbers
      do is=0,Ns-1
         xr(is)= gkiss05()
      enddo
! Real FFT of input random numbers      
       call rdft(Ns, 1, xr, ip, w)  
! Filter the Fourier components of the random numbers
! as real-space correlations are symmmetric, FT of c is real
      do is=1,Ns/2-1
          xr(2*is)=xr(2*is)*sqrt(abs(cr(2*is)))*2.D0/Ns
          xr(2*is+1)=xr(2*is+1)*sqrt(abs(cr(2*is)))*2.D0/Ns
      enddo
      xr(0)=xr(0)*sqrt(abs(cr(0)))*2.D0/Ns
      xr(1)=xr(1)*sqrt(abs(cr(1)))*2.D0/Ns 
      
! FFT of filtrered random numbers (back to real space)
       call rdft(Ns, -1, xr, ip, w)  
       
! Transform from Gaussian distribution to flat distribution on (0,1)      
!      do is = 0,Ns-1
!        xr(is) = 1 -  0.5D0*erfcc(xr(is)/sqrt(2.0D0)) 
!      end do

      deallocate(cr,ip,w)      
      return
      END SUBROUTINE corvec 



      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! FBM correlation function 
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      FUNCTION fbmcorfunc(i,N,gam)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)              :: corfunc,fbmcorfunc
      real(r8b)              :: gam
      integer(i4b)           :: i,N
      integer(i4b)           :: dist
      
      dist=min(i,N-i)
      if (dist.eq.0) then 
         corfunc=1.D0
      elseif (dist.lt.1000) then   
         corfunc = 0.5D0*( dble(dist+1)**(2.D0-gam) - 2.D0*(dble(dist)**(2.D0-gam)) + dble(dist-1)**(2.D0-gam) ) 
      else 
         corfunc = (2.D0-gam)*(1.D0-gam)*dble(dist)**(-gam) 
         corfunc = corfunc + (2.D0-gam)*(1.D0-gam)*(-gam)*(-1.D0-gam)*dble(dist)**(-gam-2.D0)/12.D0          
         corfunc = 0.5D0*corfunc
      endif   

      fbmcorfunc=corfunc
      
      return
      END FUNCTION fbmcorfunc 

      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Gaussian random number generator gkiss05
!
! generates normally distributed independent random numbers
! (zero mean, variance 1) using Box-Muller method in polar form
!
! uniform random numbers provided by Marsaglia's kiss (2005 version)
!
! before using the RNG, call gkissinit(seed) to initialize
! the generator. Seed should be a positive integer.
!
!
! History:
!      v0.9     Dec  6, 2013:   first version
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      FUNCTION gkiss05()
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      real(r8b)             :: gkiss05
      real(r8b), external   :: rkiss05

      real(r8b)             :: v1,v2,s,fac
      integer(i4b)          :: iset               ! switches between members of the Box-Muller pair
      real(r8b)             :: gset
      common /gausscom/gset,iset

      if (iset.ne.1) then
        do
          v1 = 2.D0 * rkiss05() - 1.D0
          v2 = 2.D0 * rkiss05() - 1.D0
          s = v1 * v1 + v2 * v2
          if ((s<1.D0) .and. (s>0.D0)) exit
        enddo
! Box-Muller transformation creates pairs of random numbers
        fac = sqrt(-2.D0 * log(s) / s)
        gset = v1 * fac
        iset = 1
        gkiss05 = v2 * fac
      else
        iset = 0
        gkiss05 = gset
      end if
      return
      END FUNCTION gkiss05


      SUBROUTINE gkissinit(iinit)
      implicit none
      integer,parameter     :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b)          :: iinit,iset
      real(r8b)             :: gset
      common /gausscom/gset,iset

      iset=0                         ! resets the switch between the members of the Box-Muller pair
      call kissinit(iinit)           ! initializes the rkiss05 RNG
      end subroutine gkissinit


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Random number generator KISS05 after a suggestion by George Marsaglia
! in "Random numbers for C: The END?" posted on sci.crypt.random-numbers
! in 1999
!
! version as in "double precision RNGs" in  sci.math.num-analysis
! http://sci.tech-archive.net/Archive/sci.math.num-analysis/2005-11/msg00352.html
!
! The  KISS (Keep It Simple Stupid) random number generator. Combines:
! (1) The congruential generator x(n)=69069*x(n-1)+1327217885, period 2^32.
! (2) A 3-shift shift-register generator, period 2^32-1,
! (3) Two 16-bit multiply-with-carry generators, period 597273182964842497>2^59
! Overall period > 2^123
!
!
! A call to rkiss05() gives one random real in the interval [0,1),
! i.e., 0 <= rkiss05 < 1
!
! Before using rkiss05 call kissinit(seed) to initialize
! the generator by random integers produced by Park/Millers
! minimal standard LCG.
! Seed should be any positive integer.
!
! FORTRAN implementation by Thomas Vojta, vojta@mst.edu
! built on a module found at www.fortran.com
!
!
! History:
!        v0.9     Dec 11, 2010    first implementation
!        V0.91    Dec 11, 2010    inlined internal function for the SR component
!        v0.92    Dec 13, 2010    extra shuffle of seed in kissinit
!        v0.93    Aug 13, 2012    changed integer representation test to avoid data statements
!
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      FUNCTION rkiss05()
      implicit none

      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter      :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers
      real(r8b),parameter    :: am=4.656612873077392578d-10       ! multiplier 1/2^31

      real(r8b)             :: rkiss05
      integer(i4b)          :: kiss
      integer(i4b)          :: x,y,z,w              ! working variables for the four generators
      common /kisscom/x,y,z,w

      x = 69069 * x + 1327217885
      y= ieor (y, ishft (y, 13)); y= ieor (y, ishft (y, -17)); y= ieor (y, ishft (y, 5))
      z = 18000 * iand (z, 65535) + ishft (z, - 16)
      w = 30903 * iand (w, 65535) + ishft (w, - 16)
      kiss = ishft(x + y + ishft (z, 16) + w , -1)
      rkiss05=kiss*am
      END FUNCTION rkiss05


      SUBROUTINE kissinit(iinit)
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      integer,parameter     :: i4b= SELECTED_INT_KIND(8)            ! 4-byte integers

      integer(i4b) idum,ia,im,iq,ir,iinit
      integer(i4b) k,x,y,z,w,c1,c2,c3,c4
      real(r8b)    rkiss05,rdum
      parameter (ia=16807,im=2147483647,iq=127773,ir=2836)
      common /kisscom/x,y,z,w

      !!! Test integer representation !!!
      c1=-8
      c1=ishftc(c1,-3)
!     print *,c1
      if (c1.ne.536870911) then
         print *,'Nonstandard integer representation. Stoped.'
         stop
      endif

      idum=iinit
      idum= abs(1099087573 * idum)               ! 32-bit LCG to shuffle seeds
      if (idum.eq.0) idum=1
      if (idum.ge.IM) idum=IM-1

      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         x=idum+1
      else
         x=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         y=idum+1
      else
         y=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         z=idum+1
      else
         z=idum
      endif
      k=(idum)/IQ
      idum=IA*(idum-k*IQ)-IR*k
      if (idum.lt.0) idum = idum + IM
      if (idum.lt.1) then
         w=idum+1
      else
         w=idum
      endif

      rdum=rkiss05()

      return
      end subroutine kissinit



 FUNCTION erfcc(x)
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculates complementary error function
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      implicit none
      integer,parameter      :: r8b= SELECTED_REAL_KIND(P=14,R=99)   ! 8-byte reals
      real(r8b)   :: erfcc,x
      real(r8b)   :: t,z
      z=abs(x)
      t=1.D0/(1.D0+0.5D0*z)
      erfcc=t*exp(-z*z-1.26551223D0+t*(1.00002368D0+t*(.37409196D0+t*&
     &(.09678418D0+t*(-.18628806D0+t*(.27886807D0+t*(-1.13520398D0+t*&
     &(1.48851587D0+t*(-.82215223D0+t*.17087277D0)))))))))
      if (x.lt.0.D0) erfcc=2.D0-erfcc
      return
      END  
