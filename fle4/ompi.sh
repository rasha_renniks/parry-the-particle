#!/bin/bash

comment="test"
jno="15557"

echo "Enter comment:"
read comment
echo "Job Number?:"
read jno
corre="fle4-$(date +"%m.%d.%Y_%H.%M.%S")-$comment"
echo $corre
mkdir $corre
mv *.dat $corre
mv "fle4.pbs.e$jno" $corre
mv "fle4.pbs.o$jno" $corre
cp *.plt $corre
cp corremos.f90 $corre
cp fle4.pbs $corre
